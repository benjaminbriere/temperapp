<div id="menu_more">
      <i id="more_icone" onmouseup="display_more()">
            <img class="icone_menu" id="icone_more" src="img/more.png"  alt="">     
      </i>
      <div id="display_more" class="hide">
            <div id="texte-descriptif">
            <h4>Présentation</h4>
            <div class="p4">TemperApp est une application à destination des musicien·ne·s, qui a pour vocation la réactualisation 
                  de la pratique de l’accord et des tempéraments anciens. Elle constitue un outil scientifique et 
                  pédagogique favorisant l’apprentissage de l’accord à l’oreille, dont le principe se base sur la 
                  notion de rapport intervallaire. 
            </div>
            <h4> Crédit et sources</h4>
            <div class="p4">
            TemperApp est issue du travail de recherche réalisé par :
            <br>
            <br>
            <strong>Elisa Barbessi</strong>, professeur de clavecin et histoire de la musique au CRR du Grand-Avignon, à l’initiative du projet. Doctorante à l’université Sorbonne, membre d’IReMus, Elisa est directrice artistique d’ARTEMIDA.
            <br>
            <strong>Jérôme Bertier</strong>, pianiste, claveciniste et organiste, professeur au conservatoire d’Auxerre. 
            <br>
            <strong>Pierre Cazes</strong>, claveciniste. Il enseigne au CNSMDP l'histoire, la théorie et la pratique des tempéraments, et la basse continue. Il est professeur de clavecin au CRR93 (Aubervilliers/La Courneuve).
            <br>
            <strong>Franck Jedrzejewski</strong>, chercheur au CEA, docteur habilité en musicologie et philosophie. Ancien Vice-président du Collège International de Philosophie, a publié une vingtaine d'ouvrages.
            <br>
            <strong>Théodora Psychoyou</strong>, IReMus - Sorbonne Université.
            <br>
            <br>
            L’application est développée par des étudiants de l’école d’ingénieur IMAC 
            Fabian Adam, Benjamin Briere, Daphné Chamot-Rooke, Sterenn Fonseca
            encadrés par Jérôme Belloin, informaticien.
            <br>
            <br>
            Le projet bénéficie de nombreux soutiens sous la forme de ressources humaines et techniques : 
            <br>
            Le Conservatoire National Supérieur de Musique et de Danse de Paris ,
            <br>
            le laboratoire "Lutheries - Acoustique - Musique" (LAM),
            <br>
            l'Institut Jean le Rond d’Alembert,
            <br>
            IReMus,
            <br>
            Revue Musicorum,
            <br>
            l’ssociation ARTEMIDA,
            <br>
            et le Conservatoire du Grand-Avignon.
            </div>
      </div>
</div>
       
       
<nav class="nav"> <!-- BARRE NAV BOTTOM -->
      <a href="src/pages/roue.php" class="nav__link" >
            <img class="icone_menu" id="icone_menu_accordeur" src="img/menu-accorder_noir.png"  alt="">
      </a>
      <a href="src/pages/comparer.php" class="nav__link">
            <img class="icone_menu" id="icone_menu_comparer " src="img/menu-comparer_noir.png" alt="">
      </a>
      <a href="src/pages/accueil.php" class="nav__link">
            <img class="icone_menu" id="icone_menu_accueil" src="img/menu-accueil_noir.png" alt="">
      </a>
      <a href="src/pages/fiches.php" class="nav__link">
            <img class="icone_menu" id="icone_menu_fiche" src="img/menu-fiche_noir.png" alt="">
      </a>
      <a href="src/pages/apprendre.php" class="nav__link">
            <img class="icone_menu" id="icone_menu_apprendre" src="img/menu-apprendre_noir.png" alt="">
      </a>
</nav> <!-- FIN NAV -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Roue des Notes</title>
    <script type="text/javascript" src="./lib/raphael.min.js"></script>
    <script type="text/javascript" src="./lib/raphael.icons.min.js"></script>
    <script type="text/javascript" src="./lib/wheelnav.min.js"></script>
    <link rel="stylesheet" href="src/styles/style_compare.css">

</head>
<body onload="getSelect()">

  <?php
      include('../bdd/connexion.php');
  ?>

  <div id="header_page"> <!-- HEADER LOGO -->
    <h1>COMPARER</h1>

    <i id ="infos" onmouseup="display_infos()">
      <img class="icone_menu" id="icone_infos" src="img/help.png"  alt=""> 
    </i>
    <div id="display_infos" class="hide">
      <div id="texte-descriptif">
        <h5> Comment ça fonctionne ? </h5>
        <div class="p5">
          Il est possible de comparer deux tempéraments pour visualiser
          rapidement la différence de pureté des tierces et des quintes.
          Les quintes sont représentées à l'intérieur et les tierces à
          l'extérieur.
          <br>
          <br>
          Le 1er tempérament entoure la roue des notes :
          <br>
          <img class="schema" src="img/comparer_int.png"></img>
          <br>
          <br>
          Le 2e tempérament arrive ensuite.
          <br>
          <br>
          <img class="schema" src="img/comparer_ext.png"></img>
        </div>
      </div>
    </div>

</div>

  <div id="option_selection">
      <div class="option-temperament" id="option-temp1">
        <label for="temperament1">TEMPÉRAMENT 1 :  </label>
        <select name="temperament1" id="temperament1-select"></select>
      </div>
      <div class="option-temperament" id="option-temp2">
        <label for="temperament2">TEMPÉRAMENT 2 :  </label>
        <select name="temperament2" id="temperament2-select"></select>
      </div>
  </div>


  <div id="roue_comparaison">
    <div id="wheelTierce1"></div>
    <div id="wheelTierce"></div>
    <div id="roue_notes">
        <svg version="1.1" id="notes" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 142.1 141.7" style="enable-background:new 0 0 142.1 141.7;" xml:space="preserve" >
        <style type="text/css">
          .st0{stroke:#eeeeee;stroke-width:0.5;stroke-miterlimit:10;}
          .st1{fill:#000000;}
          .st2{font-family:var(--texte);font-weight: 700;}
          .st3{font-size:15px;}
        </style>

        <g id="note_G" class="note_detection" onmouseup="selected_note_change('G')">
          <g>
            <path class="st0" d="M120.6,21.2l-18.9,18.9c-5.6-5.6-12.4-9.4-19.5-11.3L89.1,3C101.6,6.3,111.4,12.1,120.6,21.2z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 93.1877 27.1337)" class="st1 st2 st3">G</text>
        </g>
        <g id="note_C" class="note_detection" onmouseup="selected_note_change('C')">
          <g>
            <path class="st0" d="M82.1,28.9c-7.4-2-15.2-2-22.5,0L52.7,3c12.5-3.4,23.9-3.4,36.4,0L82.1,28.9z"/>
          </g>
          <text transform="matrix(1 0 0 1 65.346 19.7874)" class="st1 st2 st3">C</text>
        </g>
        <g id="note_F" class="note_detection" onmouseup="selected_note_change('F')">
          <g>
            <path class="st0" d="M59.6,28.9c-7.1,1.9-13.9,5.7-19.5,11.3L21.2,21.2c9.1-9.1,19-14.8,31.5-18.2L59.6,28.9z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 38.8659 27.1337)" class="st1 st2 st3">F</text>
        </g>
        <g id="note_Bb" class="note_detection" onmouseup="selected_note_change('B♭')">
          <g>
            <path class="st0" d="M28.8,59.6L3,52.7c3.3-12.5,9.1-22.4,18.2-31.5l18.9,18.9C34.5,45.7,30.8,52.5,28.8,59.6z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 13.3349 47.1871)" class="st1 st2 st3">B♭</text>
        </g>
        <g id="note_Eb" class="note_detection" onmouseup="selected_note_change('E♭')">
          <g>
            <path class="st0" d="M28.8,82.2L3,89.1c-3.3-12.5-3.3-23.9,0-36.4l25.8,6.9C26.9,67,26.9,74.8,28.8,82.2z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 4.9911 76.6871)" class="st1 st2 st3">E♭</text>
        </g>
        <g id="note_Gd" class="note_detection" onmouseup="selected_note_change('G#')">
          <g>
            <path class="st0" d="M40.1,101.7l-18.9,18.9c-9.1-9.1-14.8-19-18.2-31.5l25.8-6.9C30.8,89.3,34.5,96.1,40.1,101.7z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 11.4179 105.1871)" class="st1 st2 st3">G♯</text>
        </g>
        <g id="note_Cd" class="note_detection" onmouseup="selected_note_change('C#')">
          <g>
            <path class="st0" d="M52.7,138.8c-12.5-3.3-22.4-9-31.5-18.2l18.9-18.9c5.6,5.6,12.4,9.4,19.5,11.3L52.7,138.8z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 33.1879 125.9371)" class="st1 st2 st3">C♯</text>
        </g>
        <g id="note_Fd" class="note_detection" onmouseup="selected_note_change('F#')">
          <g>
            <path class="st0" d="M89.1,138.8c-12.5,3.3-23.9,3.3-36.4,0l6.9-25.8c7.4,2,15.2,2,22.5,0L89.1,138.8z"/>
          </g>
          <text transform="matrix(1 0 0 1 61.596 133.2874)" class="st1 st2 st3">F♯</text>
        </g>
        <g id="note_B" class="note_detection" onmouseup="selected_note_change('B')">
          <g>
            <path class="st0" d="M120.6,120.6c-9.1,9.1-19,14.8-31.5,18.2l-6.9-25.9c7.1-1.9,13.9-5.7,19.5-11.3L120.6,120.6z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 93.1879 125.9371)" class="st1 st2 st3">B</text>
        </g>
        <g id="note_E" class="note_detection" onmouseup="selected_note_change('E')">
          <g>
            <path class="st0" d="M120.6,120.6l-18.9-18.9c5.6-5.6,9.4-12.4,11.3-19.5l25.9,6.9C135.4,101.6,129.7,111.4,120.6,120.6z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 115.2408 105.1871)" class="st1 st2 st3">E</text>
        </g>
        <g id="note_A" class="note_detection" onmouseup="selected_note_change('A')">
          <g>
            <path class="st0" d="M138.8,89.1l-25.9-6.9c2-7.4,2-15.2,0-22.5l25.9-6.9C142.1,65.2,142.1,76.6,138.8,89.1z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 121.9911 76.6871)" class="st1 st2 st3">A</text>
        </g>
        <g id="note_D" class="note_detection" onmouseup="selected_note_change('D')">
          <g>
            <path class="st0" d="M138.8,52.7l-25.9,6.9c-1.9-7.1-5.7-13.9-11.3-19.5l18.9-18.9C129.7,30.3,135.4,40.2,138.8,52.7z"/>
          </g>
          <text transform="matrix(1 -1.677149e-03 1.677149e-03 1 115.2404 47.1871)" class="st1 st2 st3">D</text>
        </g>
      </svg>
    </div>
    <div id="wheelQuinte"></div>
    <div id="wheelQuinte1"></div>
    <div id="middle_wheel"></div>
  </div>
<!--
    <div id="wheelCache"></div>
-->

  <div class="table_commas" id="comparaison_commas">
  <table>
    <caption>COMMAS</caption>
    <thead>
      <th scope="row"></th>
      <th scope="row" id='comma_temp1'>Égal</th>
      <th scope="row" id='comma_temp2'>Égal</th>
    </thead>
    <tbody>
      <tr>
        <th scope="col">F</th>
        <td id='comF'>-1/12</td>
        <td id='com_F'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">C</th>  
        <td id='comC'>-1/12</td>
        <td id='com_C'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">G</th>
        <td id='comG'>-1/12</td>
        <td id='com_G'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">D</th>
        <td id='comD'>-1/12</td>
        <td id='com_D'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">A</th>
        <td id='comA'>-1/12</td>
        <td id='com_A'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">E</th>
        <td id='comE'>-1/12</td>
        <td id='com_E'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">B</th>
        <td id='comB'>-1/12</td>
        <td id='com_B'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">F♯</th>
        <td id='comFd'>-1/12</td>
        <td id='com_Fd'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">C♯</th>
        <td id='comCd'>-1/12</td>
        <td id='com_Cd'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">G♯</th>
        <td id='comGd'>-1/12</td>
        <td id='com_Gd'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">E♭</th>
        <td id='comEb'>-1/12</td>
        <td id='com_Eb'>-1/12</td>
      </tr>
      <tr>
        <th scope="col">B♭</th>
        <td id='comBb'>-1/12</td>
        <td id='com_Bb'>-1/12</td>
      </tr>
    </tbody>
  </table>
  </div>

  </div>

<script type="text/javascript" src="src/scripts/calcul_temperaments.js"></script>
<script type="text/javascript" src="src/scripts/compare.js">

</script>

</body>
</html>


  <link rel="stylesheet" href="src/styles/style_roue.css">

  <?php
      include('../bdd/connexion.php');
  ?>

	<div id="header_page"> <!-- HEADER LOGO -->
		<h1>ACCORDEUR</h1>
		<i id ="infos" onmouseup="display_infos()">
			<img class="icone_app" id="icone_infos" src="img/help.png"  alt="">
		</i>
			
		<div id="display_infos" class="hide">
			<div id="texte-descriptif">
				<h5> Explications </h5>
				
				<div class="p5">
					<div class="h7">DÉGRADÉS QUINTES/TIERCES</div>
					Le visuel TemperApp vous permet de prévoir en un coup d'œil la sonorité de tous les accords majeurs dans le tempérament sélectionné.
					Chaque note du cycle des quintes représente la tonalité majeure correspondante. Pour chaque accord majeur, les qualités des quintes et des tierces sont représentées par une couleur. Les tierces sont représentées à l’extérieur du cycle, les quintes à l’intérieur.
					Plus la couleur de l’intervalle est sombre, plus celui-ci est tempéré. Les intervalles au tempérament égal sont représentés avec la même couleur. 
					Seuls les intervalles purs sont colorés en bleu. Les intervalles aux tempéraments exceptionnels (quintes élargies, tierces plus grandes que pythagoricienne) sont colorés en nuances de gris.				<br>
					<br>
					Légende des couleurs :
					<br>
					<img id="illustration_gradient" src="img/explication_gradients.png" alt="">
					
					<div class="h7">PITCH PIPE & BATTEMENT</div>
					TemperApp propose deux accordeurs électroniques : 
					La “pitch pipe” émet la fréquence de la note à accorder. 
					Le “métronome par battement” émet le battement prévu d’un intervalle, sur le même principe que celui émis par l’instrument : les fréquences des harmoniques les plus proches sont émises simultanément.
					
					<div class="h7">ACCORDER</div>
					TemperApp vous accompagne dans l’accord de votre instrument. 
					Pour chaque tempérament, l’application vous propose une procédure d’accord. 
					A chaque étape vous êtes invité à accorder une note qui clignotera sur le cycle des quintes. 
					Cette note sera accordée comme un intervalle sur une note indiquée en surbrillance. 
					Le battement de  l’intervalle est affiché au centre du cercle et généré par l’application. 
					Toucher le centre du cercle permet de générer la note à accorder. A tout moment vous pouvez aller à l’étape précédente, suivante ou interrompre la procédure.
				</div>
			</div>
		  </div>
		  
</div>
		




  <div id="option_selection">
    <div id="gamme_selection">
      <div class="gamme-option" id="option-temperament">
        <label for="temperament">TEMPÉRAMENT</label>
        <select name="temperament" id="temperament-select"></select>
      </div>
      <div class="gamme-option" id="option-la">
        <label for="frequence_la">LA (Hz)</label>
        <input type="number" id="frequence_la" name="frequence_la" value="440">
      </div>
    </div>
  </div>

  <div id="note_selection">

    <!--OSKOUR-->

    <div id="gestion_notes_octave" style='width:92vw; height:92vw;'>
          <svg version="1.1" id="notes_octave" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           viewBox="0 0 142.1 141.7" style="enable-background:new 0 0 142.1 141.7;" xml:space="preserve" >
          <style type="text/css">
          .st4{fill:#c2c2c2;}
          .st6{font-family:'Karla',sans-serif;}
          .st5{font-size:10px;}
          </style>

          <g id="notes_x5F_octave">
          	<g id="note_F3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('F')">
          		<g>
          			<path class="st2" d="M52.8,2.9l4.2,15.8c-9.5,2.5-17.7,7.5-24.2,14L21.3,21.1C30.4,12,40.3,6.2,52.8,2.9z"/>
          		</g>
          		<text transform="matrix(0.8628 -0.5056 0.5056 0.8628 37.4386 22.3227)" class="st5 st4 st6">F3</text>
          	</g>
          	<g id="note_Bb3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('B♭')">
          		<g>
          			<path class="st2" d="M32.9,32.7c-6.7,6.7-11.5,15.1-14,24.2L3.1,52.7c3.3-12.5,9-22.4,18.2-31.5L32.9,32.7z"/>
          		</g>
          		<text transform="matrix(0.4694 -0.883 0.883 0.4694 16.4774 48.7934)" class="st5 st4 st6">B♭3</text>
          	</g>
          	<g id="note_Eb3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('E♭')">
          		<g>
          			<path class="st2" d="M18.9,56.9c-2.4,8.9-2.5,18.4,0,27.9L3.1,89.1c-3.4-12.5-3.4-23.9,0-36.4L18.9,56.9z"/>
          		</g>
          		<text transform="matrix(-2.602255e-03 -1 1 -2.602255e-03 13.0897 79.863)" class="st5 st4 st6">E♭3</text>
          	</g>
          	<g id="note_Gd3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('G#')">
          		<g>
          			<path class="st2" d="M32.9,109l-11.6,11.6c-9.2-9.1-14.9-19-18.2-31.5l15.8-4.2C21.5,94.3,26.4,102.5,32.9,109z"/>
          		</g>
          		<text transform="matrix(0.5852 0.8109 -0.8109 0.5852 8.1312 96.2588)" class="st5 st4 st6">G♯3</text>
          	</g>
          	<g id="note_Cd3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('C#')">
          		<g>
          			<path class="st2" d="M57.1,123l-4.2,15.8c-12.5-3.3-22.4-9.1-31.5-18.2L32.9,109C39.6,115.7,48,120.5,57.1,123z"/>
          		</g>
          		<text transform="matrix(0.8585 0.5129 -0.5129 0.8585 28.2323 122.1116)" class="st5 st4 st6">C♯3</text>
          	</g>
          	<g id="note_Fd3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('F#')">
          		<g>
          			<path class="st2" d="M85,122.9l4.2,15.8c-12.5,3.4-23.9,3.4-36.4,0l4.2-15.8C66,125.3,75.5,125.5,85,122.9z"/>
          		</g>
          		<text transform="matrix(1 0 0 1 61.7493 136.5426)" class="st5 st4 st6">F♯3</text>
          	</g>
          	<g id="note_B3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('B')">
          		<g>
          			<path class="st2" d="M120.8,120.6c-9.2,9.2-19,14.9-31.5,18.2L85,122.9c9.5-2.5,17.7-7.5,24.2-14L120.8,120.6z"/>
          		</g>
          		<text transform="matrix(0.8678 -0.497 0.497 0.8678 97.9248 131.299)" class="st5 st4 st6">B3</text>
          	</g>
          	<g id="note_E3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('E')">
          		<g>
          			<path class="st2" d="M139,89c-3.3,12.5-9.1,22.4-18.2,31.5L109.2,109c6.7-6.7,11.5-15.1,14-24.2L139,89z"/>
          		</g>
          		<text transform="matrix(0.5093 -0.8606 0.8606 0.5093 124.7582 109.0068)" class="st5 st4 st6">E3</text>
          	</g>
          	<g id="note_A3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('A')">
          		<g>
          			<path class="st2" d="M139,52.6c3.4,12.5,3.4,23.9,0,36.4l-15.8-4.2c2.4-8.9,2.5-18.4,0-27.9L139,52.6z"/>
          		</g>
          		<text transform="matrix(-4.642056e-02 0.9989 -0.9989 -4.642056e-02 128.9093 64.2757)" class="st5 st4 st6">A3</text>
          	</g>
          	<g id="note_D3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('D')">
          		<g>
          			<path class="st2" d="M139,52.6l-15.8,4.2c-2.5-9.5-7.5-17.7-14-24.2l11.6-11.6C129.9,30.2,135.6,40.1,139,52.6z"/>
          		</g>
          		<text transform="matrix(0.5795 0.815 -0.815 0.5795 117.3805 35.9168)" class="st5 st4 st6">D3</text>
          	</g>
          	<g id="note_G3" class="note_detection_octave octave_cache" onmouseup="selected_note_change_octave('G')">
          		<g>
          			<path class="st2" d="M120.8,21.1l-11.6,11.6c-6.7-6.7-15.1-11.5-24.2-14l4.2-15.8C101.7,6.2,111.6,11.9,120.8,21.1z"/>
          		</g>
          		<text transform="matrix(0.8713 0.4908 -0.4908 0.8713 93.5239 16.7276)" class="st5 st4 st6">G3</text>
          	</g>
          	<g id="note_C3" class="note_detection_octave " onmouseup="selected_note_change_octave('C')">
          		<g>
          			<path class="st2" d="M89.2,2.9L85,18.7c-8.9-2.4-18.4-2.5-27.9,0L52.8,2.9C65.3-0.5,76.7-0.5,89.2,2.9z"/>
          		</g>
          		<text transform="matrix(1 0 0 1 65.0827 12.8758)" class="st5 st4 st6">C3</text>
          	</g>
          </g>
          </svg>
        </div>

    <div id="gestion_notes" style='width:70vw; height:70vw;'>
      <svg version="1.1" id="notes" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
  	   viewBox="0 0 142.1 141.7" style="enable-background:new 0 0 142.1 141.7;" xml:space="preserve" >
      <style type="text/css">
	  	.st0{stroke:#eeeeee;stroke-width:0.5;stroke-miterlimit:10;}
		.st1{fill:#000000;}
		.st2{font-family:'Karla', sans-serif;font-weight: 700;}
		.st3{font-size:15px;}
      </style>

      <g id="note_G4" class="note_detection" onmouseup="selected_note_change('G')">
      	<g>
      		<path class="st0" d="M120.6,21.2l-18.9,18.9c-5.6-5.6-12.4-9.4-19.5-11.3L89.1,3C101.6,6.3,111.4,12.1,120.6,21.2z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 93.1877 27.1337)" class="st1 st2 st3">G</text>
      </g>
      <g id="note_C4" class="note_detection current_note" onmouseup="selected_note_change('C')">
      	<g>
      		<path class="st0" d="M82.1,28.9c-7.4-2-15.2-2-22.5,0L52.7,3c12.5-3.4,23.9-3.4,36.4,0L82.1,28.9z"/>
      	</g>
      	<text transform="matrix(1 0 0 1 65.346 19.7874)" class="st1 st2 st3">C</text>
      </g>
      <g id="note_F4" class="note_detection" onmouseup="selected_note_change('F')">
      	<g>
      		<path class="st0" d="M59.6,28.9c-7.1,1.9-13.9,5.7-19.5,11.3L21.2,21.2c9.1-9.1,19-14.8,31.5-18.2L59.6,28.9z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 38.8659 27.1337)" class="st1 st2 st3">F</text>
      </g>
      <g id="note_Bb4" class="note_detection" onmouseup="selected_note_change('B♭')">
      	<g>
      		<path class="st0" d="M28.8,59.6L3,52.7c3.3-12.5,9.1-22.4,18.2-31.5l18.9,18.9C34.5,45.7,30.8,52.5,28.8,59.6z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 13.3349 47.1871)" class="st1 st2 st3">B♭</text>
      </g>
      <g id="note_Eb4" class="note_detection" onmouseup="selected_note_change('E♭')">
      	<g>
      		<path class="st0" d="M28.8,82.2L3,89.1c-3.3-12.5-3.3-23.9,0-36.4l25.8,6.9C26.9,67,26.9,74.8,28.8,82.2z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 4.9911 76.6871)" class="st1 st2 st3">E♭</text>
      </g>
      <g id="note_Gd4" class="note_detection" onmouseup="selected_note_change('G#')">
      	<g>
      		<path class="st0" d="M40.1,101.7l-18.9,18.9c-9.1-9.1-14.8-19-18.2-31.5l25.8-6.9C30.8,89.3,34.5,96.1,40.1,101.7z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 11.4179 105.1871)" class="st1 st2 st3">G♯</text>
      </g>
      <g id="note_Cd4" class="note_detection" onmouseup="selected_note_change('C#')">
      	<g>
      		<path class="st0" d="M52.7,138.8c-12.5-3.3-22.4-9-31.5-18.2l18.9-18.9c5.6,5.6,12.4,9.4,19.5,11.3L52.7,138.8z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 33.1879 125.9371)" class="st1 st2 st3">C♯</text>
      </g>
      <g id="note_Fd4" class="note_detection" onmouseup="selected_note_change('F#')">
      	<g>
      		<path class="st0" d="M89.1,138.8c-12.5,3.3-23.9,3.3-36.4,0l6.9-25.8c7.4,2,15.2,2,22.5,0L89.1,138.8z"/>
      	</g>
      	<text transform="matrix(1 0 0 1 61.596 133.2874)" class="st1 st2 st3">F♯</text>
      </g>
      <g id="note_B4" class="note_detection" onmouseup="selected_note_change('B')">
      	<g>
      		<path class="st0" d="M120.6,120.6c-9.1,9.1-19,14.8-31.5,18.2l-6.9-25.9c7.1-1.9,13.9-5.7,19.5-11.3L120.6,120.6z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 93.1879 125.9371)" class="st1 st2 st3">B</text>
      </g>
      <g id="note_E4" class="note_detection" onmouseup="selected_note_change('E')">
      	<g>
      		<path class="st0" d="M120.6,120.6l-18.9-18.9c5.6-5.6,9.4-12.4,11.3-19.5l25.9,6.9C135.4,101.6,129.7,111.4,120.6,120.6z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 115.2408 105.1871)" class="st1 st2 st3">E</text>
      </g>
      <g id="note_A4" class="note_detection" onmouseup="selected_note_change('A')">
      	<g>
      		<path class="st0" d="M138.8,89.1l-25.9-6.9c2-7.4,2-15.2,0-22.5l25.9-6.9C142.1,65.2,142.1,76.6,138.8,89.1z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 121.9911 76.6871)" class="st1 st2 st3">A</text>
      </g>
      <g id="note_D4" class="note_detection" onmouseup="selected_note_change('D')">
      	<g>
      		<path class="st0" d="M138.8,52.7l-25.9,6.9c-1.9-7.1-5.7-13.9-11.3-19.5l18.9-18.9C129.7,30.3,135.4,40.2,138.8,52.7z"/>
      	</g>
      	<text transform="matrix(1 -1.677149e-03 1.677149e-03 1 115.2404 47.1871)" class="st1 st2 st3">D</text>
      </g>
      </svg>
    </div>


    <div id="wheelTierce"></div>
    <div id="wheelQuinte"></div>

    <div id="selected_note">
      <p id="selected_note_content">C<sub>4</sub></p>
      <p id="selected_note_frequency"></p>
    </div>

	<div id="buttons_container">
		<div id="onoff-container">
			<div id="on_off" class="off" onmouseup="change_on_off()">
				<img class="icone_app" id="icone_on_off" src="img/accordeur/son_off.png" alt=""></img>
			</div>
		</div>

		<div id="mode-container">
			<div id="mode_accordeur" class="diapason" onmouseup="change_accordeur_mode()">
				<img class="icone_app" id="icone_mode" src="img/accordeur/logo_diap.png" alt=""></img>
			</div>
		</div>
	</div>

  <div id="battement_procedure_next_container">
    <div id="battement_procedure_next" class="arrow_procedure" onmouseup="battement_procedure_next()"><p>></p></div>
  </div>

  <div id="battement_step_container">
    <div id="battement_procedure_step" class="arrow_procedure" onmouseup="battement_procedure_step()">
      <p id="battement_step">1</p>
      <p id="battement_type_procedure">accorder</p>
    </div>
  </div>

  <div id="battement_procedure_previous_container">
    <div id="battement_procedure_previous" class="arrow_procedure" onmouseup="battement_procedure_previous()"><p><</p></div>
  </div>

  </div>

<script type="text/javascript" src="src/scripts/calcul_temperaments.js"></script>
<script type="text/javascript" src="src/scripts/roue.js"></script>
<script type="text/javascript" src="src/scripts/battement.js"></script>


        <div id="header_page"> <!-- HEADER LOGO --> 
            <div>
            <h1>FICHES<h1>
            <div id="separateur"></div>
            </div>
            
        </div>
        
    
        <div id="partie_recherche">
            <div class="barre_de_recherche">
                <input type="text" class="searchTerm" placeholder="Tempérament, compositeur, époque..." onkeyup="recherche(this.value)">
                <button type="submit" class="searchButton"><img src="img/recherche_noir.png" alt="" width="15px" height="15px"></button>
            </div>
        </div>
        

        <div id="partie_marquepage"> <!-- PARTIE 1 -->
            <div class="sous_titre">
                <h2>TOUS LES TEMPÉRAMENTS</h2>
            </div>
            <div class="conteneur_marquepage" id="conteneur_tous">

            <!-- avant
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Mésotonique 1/4 Cs</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Vallotti</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Rameau II</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Rameau III</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Mesotonique 1/3</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Bach</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Rameau II</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Rameau III</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Mesotonique 1/3</h6>
                </div>
                <div class="marquepage1">
                    <img class="temperament_marque_page mp--active" src="img/marque-page_noir.png" alt="">
                    <h6>Bach</h6>
                </div>
            
            </div> avant -->
            
        </div> <!-- FIN 1 -->

        <script type="text/javascript" src="src/scripts/fiches.js"></script>
        
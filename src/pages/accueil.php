
<!--<link rel="stylesheet" type="text/css" href="src/styles/style_accueil.css"/>-->
    <div id="header"> <!-- HEADER LOGO -->
        <img id="titre_TemperApp" src="img/titre_noir.png" alt="">
    </div>
    <div id="partie_marquepage"> <!-- PARTIE MARQUE PAGE -->
        <div class="sous-titre">
            <h2>LES + UTILISÉS</h2>
            <img id="icone_marque_page" src="img/marque-page_noir.png" alt="">
        </div>

    <!-- SUR L'ACCUEIL, LES DIFFERENTS TEMPERAMENTS N'AURONT PAS DE MARQUE PAGE
    DONC ON AJOUTE .mp--jamais DERRIERE LA CLASSE : CELUI CI RAJOUTE UN PADDING-TOP -->


        <div class="conteneur_marquepage" id="accueil_temp">
            <div class="marquepage1 mp--jamais">
            </div>
            <div class="marquepage1 mp--jamais">
            </div>
            <div class="marquepage1 mp--jamais">
            </div>
            <div class="marquepage1 mp--jamais">
            </div>
            <div class="marquepage1 mp--jamais">
            </div>
            <div class="marquepage1 mp--jamais">
            </div>
        </div>
    </div>  <!-- FIN MARQUE PAGE -->

    <div id="bas_ecran">
        <div id="partie_raccourci_accord"> <!-- PARTIE RACCOURCI VERS ACCORD -->
            <div class="conteneur_bouton_accord">
                <div id="cercle_bouton_accordeur">
                    <div class="conteneur_image_texte">
                    <a class="link" href="src/pages/roue.php">
                        <img id="logo_accordeur" src="img/menu-accorder_noir.png"  alt="bouton accordeur" >
                        <div id="texte_bouton">ACCORDEUR</div></a>
                    </div>
                </div>
            </div>
        </div> <!-- FIN RACCOURCI VERS ACCORD -->
    </div>

    <script type="text/javascript" src="src/scripts/accueil.js"></script>
    <script type="text/javascript" src="src/scripts/menu.js"></script>



UpdateHTML_accueil();


function UpdateHTML_accueil(){
    console.log('----------------- FICHE - AFFICHAGE DES TEMPERAMENTS -------------------');
    const temp_boite = document.getElementById("accueil_temp");
    var message = "";
    let params = {};

    fetch('./src/bdd/liste_temperaments.php', {method: 'POST'})
    .then(response => response.json())
    .then( data => {
      var html = "";
      data.temperaments.forEach(function(temperament){
                console.log(temperament.nom);
                let function_string = "onmouseup="+"affichage_fiche_individuelle("+temperament.id+",\'"+temperament.nom+"\')";
                console.log(function_string);
                html += "\
                <div  class='marquepage1' id="+temperament.id+" "+function_string+">\
                     <img class='temperament_marque_page' src='img/marque-page_noir.png'  alt=''>\
                     <h6>"+temperament.nom+"</h6>\
                 </div>"
        })
        temp_boite.innerHTML = html;
      })
}
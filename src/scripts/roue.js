// LES VARIABLES GLOBALES //

var tab_temperament;
var temperament_name = document.getElementById('temperament-select');
var frequence_la = document.getElementById('frequence_la');
var temperament_egal;
var temperament_calcule;
var current_note;
var selected_note_frequency = document.getElementById('selected_note_frequency');
var on_off = document.getElementById('on_off');
var o;
var g;
var ctx;
var init_diapason = 0;
var te440 = '{ "A4": 440, "B4": 493.8833012561241 , "Bb4": 466.1637615180899, "C4": 261.62556530059874, "Cd4": 277.1826309768722, "D4": 293.66476791740763, "E4": 329.62755691287, "Eb4": 311.126983722081, "F4": 349.22823143300394, "Fd4": 369.99442271163446, "G4": 391.9954359817493, "Gd4": 415.3046975799451 } ';
var mode_accordeur = document.getElementById('mode_accordeur');
var son_en_cours = 0;
var battement_running = 0;


async function run_diapason(){
  console.log('---------------------- DIAPASON ------------------------------');
  te440 = JSON.parse(te440);
  temperament_egal = te440;
  temperament_calcule = temperament_egal;
  current_note = 'C4';
  await initHTML();
}

run_diapason();


//console.log("Suis-je dans le bon fichier ?");
//test();

temperament_name.onchange = async function (){
  console.log("CHANGEMENT TEMPERAMENT");
  console.log(temperament_name.value);
  UpdateTierce(temperament_name.value);
  UpdateQuinte(temperament_name.value);
  temperament_calcule = await calculTemperament(temperament_egal, temperament_name.value);
  console.log(temperament_calcule);
  frequency = Math.round(temperament_calcule[current_note]*100)/100+'Hz';
  o.frequency.value = Math.round(temperament_calcule[current_note]*100)/100;
  selected_note_frequency.innerHTML = frequency;
}

frequence_la.onchange = async function (){
  //console.log();
  console.log("CHANGEMENT FREQUENCE LA");
  console.log(frequence_la.value);
  if(frequence_la.value == 440.0){
    console.log("pas besoin de recalculer voyons ");
    temperament_egal = te440;
  }
  else{
    console.log("bien sur que nous recalculons")
    temperament_egal = await calculTemperamentEgal(parseFloat(frequence_la.value));
  }
  console.log(temperament_egal);
  temperament_calcule = await calculTemperament(temperament_egal, temperament_name.value );
  console.log(temperament_calcule);
  frequency = Math.round(temperament_calcule[current_note]*100)/100+'Hz';
  o.frequency.value = Math.round(temperament_calcule[current_note]*100)/100;
  selected_note_frequency.innerHTML = frequency;
}

function UpdateTierce(nom){
  let params = {};
  console.log('--- UpdateTierce');
  params.nom = nom;
  fetch('./src/bdd/qualite_tierce.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(response => response.json())
  .then( data => {
    console.log('--- Lecture qualité tierce');
    var r = document.querySelector(':root');
    data.temperaments.forEach(function(temperament){
        r.style.setProperty('--tierce1', couleurTierce(temperament.C));
        r.style.setProperty('--tierce2', couleurTierce(temperament.G));
        r.style.setProperty('--tierce3', couleurTierce(temperament.D));
        r.style.setProperty('--tierce4', couleurTierce(temperament.A));
        r.style.setProperty('--tierce5', couleurTierce(temperament.E));
        r.style.setProperty('--tierce6', couleurTierce(temperament.B));
        r.style.setProperty('--tierce7', couleurTierce(temperament.Fd));
        r.style.setProperty('--tierce8', couleurTierce(temperament.Cd));
        r.style.setProperty('--tierce9', couleurTierce(temperament.Gd));
        r.style.setProperty('--tierce10', couleurTierce(temperament.Eb));
        r.style.setProperty('--tierce11', couleurTierce(temperament.Bb));
        r.style.setProperty('--tierce12', couleurTierce(temperament.F));
    })

  })
}

function couleurTierce(n){
  if(n< 0 || n>20){
    if(n< 0){
      return '#434343';
    }
    else{
      return '#8a0000';
    }
  }
  else{
    switch (n) {
      case '0':
        return '#45cbc7';
      break;
      case '1':
      return '#ffedd0';
      break;
      case '2':
        return '#ffe9c6';
      break;
      case '3':
      return '#ffe5bc';
      break;
      case '4':
        return '#ffe1b1';
      break;
      case '5':
      return '#ffdda7';
      break;
      case '6':
        return '#ffda9e';
      break;
      case '7':
        return '#ffd694';
      break;
      case '8':
      return '#ffd289';
      break;
      case '9':
        return '#ffc77d';
      break;
      case '10':
      return '#ffa669';
      break;
      case '11':
        return '#dd8654';
      break;
      case '12':
      return '#ff6741';
      break;
      case '13':
        return '#ff472d';
      break;
      case '14':
        return '#ff2416';
      break;
      case '15':
      return '#ff0604';
      break;
      case '16':
      return '#eb0000';
      break;
      case '17':
      return '#d40000';
      break;
      case '18':
      return '#bc0000';
      break;
      case '19':
      return '#a60000';
      break;
      case '20':
      return '#8a0000';
      break;
      default:
        return '#ffa669';
    }
  }
}

function UpdateQuinte(nom){
  let params = {};
  console.log('--- UpdateQuinte');
  params.nom = nom;
  fetch('./src/bdd/qualite_quinte.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(response => response.json())
  .then( data => {
    console.log('--- Lecture qualité Quinte');
    var r = document.querySelector(':root');
    data.temperaments.forEach(function(temperament){
        r.style.setProperty('--quinte1', couleurQuinte(temperament.C));
        r.style.setProperty('--quinte2', couleurQuinte(temperament.G));
        r.style.setProperty('--quinte3', couleurQuinte(temperament.D));
        r.style.setProperty('--quinte4', couleurQuinte(temperament.A));
        r.style.setProperty('--quinte5', couleurQuinte(temperament.E));
        r.style.setProperty('--quinte6', couleurQuinte(temperament.B));
        r.style.setProperty('--quinte7', couleurQuinte(temperament.Fd));
        r.style.setProperty('--quinte8', couleurQuinte(temperament.Cd));
        r.style.setProperty('--quinte9', couleurQuinte(temperament.Gd));
        r.style.setProperty('--quinte10', couleurQuinte(temperament.Eb));
        r.style.setProperty('--quinte11', couleurQuinte(temperament.Bb));
        r.style.setProperty('--quinte12', couleurQuinte(temperament.F));
    })
  })
}

function couleurQuinte(n){
  if(n< -12 || n>7){
    if(n< -12){
      return '#4f0000';
    }
    else{
      return '#161616';
    }
  }
  else{
    switch (n) {
      case '-12':
        return '#4f0000';
      break;
      case '-11':
        return '#7b0000';
      break;
      case '-10':
        return '#ae0000';
      break;
      case '-9':
        return '#e00000';
      break;
      case '-8':
        return '#ff0d08';
      break;
      case '-7':
        return '#ff321f';
      break;
      case '-6':
        return '#ff5435';
      break;
      case '-5':
        return '#ff764a';
      break;
      case '-4':
        return '#ff9960';
      break;
      case '-3':
        return '#ffbb75';
      break;
      case '-2':
        return '#ffd592';
      break;
      case '-1':
        return '#ffe5bb';
      break;
      case '0':
        return '#45cbc7';
      break;
      case '1':
        return '#b9b9b9';
      break;
      case '2':
        return '#9f9f9f';
      break;
      case '3':
        return '#7e7e7e';
      break;
      case '4':
        return '#666666';
      break;
      case '5':
        return '#4d4d4d';
      break;
      case '6':
        return '#333333';
      break;
      case '7':
        return '#161616';
      break;
      default:
        return '#E69138';
    }
  }
}

async function initHTML(){
  await UpdateHTML();
}

function loadJSON(url) {
    return new Promise(resolve => {
      var request = new XMLHttpRequest();
      request.open('GET', url);
      request.responseType = 'json';
      request.send();
      var result;
      var tab;
      request.onload = function() {
        result = request.response;
        tab = result;
        console.log(result);
        resolve(result);
      }
    });
 }

function UpdateHTML(){
   var message = "";
   let params = {};
   fetch('./src/bdd/liste_temperaments.php', {method: 'POST'})
   .then(response => response.json())
   .then( data => {

     data.temperaments.forEach(function(temperament){
         const info = "<option value ="+temperament.nom+">"+temperament.nom+"</option>";
         message = message + info;
     })
     temperament_name.innerHTML  = message;
   })
   UpdateQuinte('Égal');
   UpdateTierce('Égal');
   frequency = Math.round(temperament_calcule['C4']*100)/100+'Hz';
   selected_note_frequency.innerHTML = frequency;

   diapason(Math.round(temperament_calcule['C4']*100)/100);
   g.gain.value = 0;
 }

/*
function affect_note(){
  for (var i = 0; i < 12; i++) {
    var name_id = 'wheelnav-wheelDiv-slice-'+i;
    var element = document.getElementById(name_id);

    switch (i) {

    case 0:
    element.classList.add('A');
    break;

    case 1:
    element.classList.add('E');
    break;

    case 2:
    element.classList.add('B');
    break;

    case 3:
    element.classList.add('F#');
    break;

    case 4:
    element.classList.add('C#');
    break;

    case 5:
    element.classList.add('G#');
    break;

    case 6:
    element.classList.add('E♭');
    break;

    case 7:
    element.classList.add('B♭');
    break;

    case 8:
    element.classList.add('F');
    break;

    case 9:
    element.classList.add('C');
    break;

    case 10:
    element.classList.add('G');
    break;

    case 11:
    element.classList.add('D');
    break;

    default:
      element.classList.add('A');
    }

    //console.log(name_id);
    //console.log(element);
  }
}
*/
function conversion_note_octave4(id){
  switch (id) {
  case 'A':
  return 'A<sub>4</sub>';
  break;
  case 'B':
  return 'B<sub>4</sub>';
  break;
  case 'B♭':
  return 'B♭<sub>4</sub>';
  break;
  case 'C':
  return 'C<sub>4</sub>';
  break;
  case 'C#':
  return 'C♯<sub>4</sub>';
  break;
  case 'D':
  return 'D<sub>4</sub>';
  break;
  case 'E':
  return 'E<sub>4</sub>';
  break;
  case 'E♭':
  return 'E♭<sub>4</sub>';
  break;
  case 'F':
  return 'F<sub>4</sub>';
  break;
  case 'F#':
  return 'F♯<sub>4</sub>';
  break;
  case 'G':
  return 'G<sub>4</sub>';
  break;
  case 'G#':
  return 'G♯<sub>4</sub>';
  break;
    default:
    return '?';
  }
}

function conversion_note_octave3(id){
  switch (id) {
  case 'A':
  return 'A<sub>3</sub>';
  break;
  case 'B':
  return 'B<sub>3</sub>';
  break;
  case 'B♭':
  return 'B♭<sub>3</sub>';
  break;
  case 'C':
  return 'C<sub>3</sub>';
  break;
  case 'C#':
  return 'C♯<sub>3</sub>';
  break;
  case 'D':
  return 'D<sub>3</sub>';
  break;
  case 'E':
  return 'E<sub>3</sub>';
  break;
  case 'E♭':
  return 'E♭<sub>3</sub>';
  break;
  case 'F':
  return 'F<sub>3</sub>';
  break;
  case 'F#':
  return 'F♯<sub>3</sub>';
  break;
  case 'G':
  return 'G<sub>3</sub>';
  break;
  case 'G#':
  return 'G♯<sub>3</sub>';
  break;
    default:
    return '?';
  }
}

function selected_note_change(id){
  //console.log(id);
  note= document.getElementById('selected_note_content');
  note.innerHTML = conversion_note_octave4(id);
  let frequency = "0Hz";
  let ex_current = 'note_'+current_note;
  //console.log(ex_current);
  let ex_current_id = document.getElementById(ex_current);
  let octave = ex_current.substring(0, ex_current.length - 1);
  octave = octave+'3';
  document.getElementById(octave).classList.add('octave_cache');
  ex_current_id.classList.remove('current_note');
  if (son_en_cours == 1){
    o.stop();
    son_en_cours = 0;
  }

  if(id == 'A'){
    let current = document.getElementById('note_A4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['A4']*100)/100+"Hz";
    current_note = 'A4';
    diapason(temperament_calcule['A4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_A3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }

  }
  if(id == 'B♭'){
    let current = document.getElementById('note_Bb4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Bb4']*100)/100+"Hz";
    current_note = 'Bb4';
    diapason(temperament_calcule['Bb4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Bb3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'B'){
    let current = document.getElementById('note_B4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['B4']*100)/100+"Hz";
    current_note = 'B4';
    diapason(temperament_calcule['B4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_B3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'C'){
    let current = document.getElementById('note_C4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['C4']*100)/100+"Hz";
    current_note = 'C4';
    diapason(temperament_calcule['C4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_C3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'C#'){
    let current = document.getElementById('note_Cd4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Cd4']*100)/100+"Hz";
    current_note = 'Cd4';
    diapason(temperament_calcule['Cd4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Cd3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'D'){
    let current = document.getElementById('note_D4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['D4']*100)/100+"Hz";
    current_note = 'D4';
    diapason(temperament_calcule['D4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_D3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'E♭'){
    let current = document.getElementById('note_Eb4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Eb4']*100)/100+"Hz";
    current_note = 'Eb4';
    diapason(temperament_calcule['Eb4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Eb3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'E'){
    let current = document.getElementById('note_E4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['E4']*100)/100+"Hz";
    current_note = 'E4';
    diapason(temperament_calcule['E4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_E3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'F'){
    let current = document.getElementById('note_F4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['F4']*100)/100+"Hz";
    current_note = 'F4';
    diapason(temperament_calcule['F4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_F3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'F#'){
    let current = document.getElementById('note_Fd4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Fd4']*100)/100+"Hz";
    current_note = 'Fd4';
    diapason(temperament_calcule['Fd4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Fd3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'G'){
    let current = document.getElementById('note_G4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['G4']*100)/100+"Hz";
    current_note = 'G4';
    diapason(temperament_calcule['G4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_G3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  if(id == 'G#'){
    let current = document.getElementById('note_Gd4');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Gd4']*100)/100+"Hz";
    current_note = 'Gd4';
    diapason(temperament_calcule['Gd4']*100 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Gd3';
    document.getElementById(octave_current).classList.remove('octave_cache');
    if (document.getElementById(octave_current).classList.contains('current_note')){
      document.getElementById(octave_current).classList.remove('current_note');
    }
  }
  selected_note_frequency.innerHTML = frequency;

}

function selected_note_change_octave(id){
  //console.log(id);
  note= document.getElementById('selected_note_content');
  note.innerHTML = conversion_note_octave3(id);
  let frequency = "0Hz";
  let ex_current = 'note_'+current_note;
  //console.log(ex_current);
  let ex_current_id = document.getElementById(ex_current);
  let octave = ex_current.substring(0, ex_current.length - 1);
  octave = octave+'3';
  document.getElementById(octave).classList.add('octave_cache');
  ex_current_id.classList.remove('current_note');
  if (son_en_cours == 1){
    o.stop();
    son_en_cours = 0;
  }

  if(id == 'A'){
    let current = document.getElementById('note_A3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['A4']*100)/2/100+"Hz";
    current_note = 'A3';
    diapason(temperament_calcule['A4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_A3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'B♭'){
    let current = document.getElementById('note_Bb3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Bb4']*100)/2/100+"Hz";
    current_note = 'Bb3';
    diapason(temperament_calcule['Bb4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Bb3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'B'){
    let current = document.getElementById('note_B3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['B4']*100)/2/100+"Hz";
    current_note = 'B3';
    diapason(temperament_calcule['B4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_B3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'C'){
    let current = document.getElementById('note_C3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['C4']*100)/2/100+"Hz";
    current_note = 'C3';
    diapason(temperament_calcule['C4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_C3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'C#'){
    let current = document.getElementById('note_Cd3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Cd4']*100)/2/100+"Hz";
    current_note = 'Cd3';
    diapason(temperament_calcule['Cd4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Cd3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'D'){
    let current = document.getElementById('note_D3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['D4']*100)/2/100+"Hz";
    current_note = 'D3';
    diapason(temperament_calcule['D4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_D3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'E♭'){
    let current = document.getElementById('note_Eb3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Eb4']*100)/2/100+"Hz";
    current_note = 'Eb3';
    diapason(temperament_calcule['Eb4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Eb3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'E'){
    let current = document.getElementById('note_E3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['E4']*100)/2/100+"Hz";
    current_note = 'E3';
    diapason(temperament_calcule['E4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_E3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'F'){
    let current = document.getElementById('note_F3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['F4']*100)/2/100+"Hz";
    current_note = 'F3';
    diapason(temperament_calcule['F4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_F3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'F#'){
    let current = document.getElementById('note_Fd3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Fd4']*100)/2/100+"Hz";
    current_note = 'Fd3';
    diapason(temperament_calcule['Fd4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Fd3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'G'){
    let current = document.getElementById('note_G3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['G4']*100)/2/100+"Hz";
    current_note = 'G3';
    diapason(temperament_calcule['G4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_G3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  if(id == 'G#'){
    let current = document.getElementById('note_Gd3');
    current.classList.add('current_note');
    frequency = Math.round(temperament_calcule['Gd4']*100)/2/100+"Hz";
    current_note = 'Gd3';
    diapason(temperament_calcule['Gd4']*100/2 /100);
    if (on_off.classList.contains('off')){
      g.gain.value = 0;
    }
    else{
      g.gain.value = 1;
    }
    let octave_current = 'note_Gd3';
    if(document.getElementById(octave_current).classList.contains('octave_cache')){
      document.getElementById(octave_current).classList.remove('octave_cache');
    }
  }
  selected_note_frequency.innerHTML = frequency;
}

function change_on_off(){
  if (on_off.classList.contains('off')){
    on_off.classList.remove('off');
    on_off.classList.add('on');
    document.images["icone_on_off"].src="img/accordeur/son_on.png";
    console.log(g.gain.value);
    g.gain.value = 1;
  }
  else{
    on_off.classList.remove('on');
    on_off.classList.add('off');
    document.images["icone_on_off"].src="img/accordeur/son_off.png";
    console.log(g.gain.value);
    g.gain.value = 0;
  }
}

function diapason(freq){
  son_en_cours = 1;
  console.log(freq);
  console.log('création de l audio context');
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  ctx = new AudioContext();
  o = ctx.createOscillator();
  o.type = "sine";
  o.frequency.value = freq;
  g = ctx.createGain();
  g.gain.value = 1;
  o.start(0);
  o.connect(g);
  g.connect(ctx.destination);
  ctx.resume();
}

function change_accordeur_mode(){
  if (mode_accordeur.classList.contains('diapason')){
    mode_accordeur.classList.remove('diapason');
    mode_accordeur.classList.add('battements');

    if(son_en_cours == 1){
      g.gain.value = 0;
      o.stop();
      son_en_cours = 0;
    }

    console.log('merde ');
    document.images["icone_mode"].src="img/accordeur/logo_batt.png";
    let next = document.getElementById('battement_procedure_next');
    next.style.display="flex";
    let step = document.getElementById('battement_procedure_step');
    step.style.display="flex";
    let previous = document.getElementById('battement_procedure_previous');
    previous.style.display="flex";
    console.log("battements");

    //NETTOYAGE de la div diapason
    let elements = document.getElementsByClassName('note_detection');
    for (let i = 0; i < elements.length; i++) {
        if(elements[i].classList.contains('current_note')){
          elements[i].classList.remove('current_note');
        }
    }

    elements = document.getElementsByClassName('note_detection_octave');
    for (let i = 0; i < elements.length; i++) {
        if(elements[i].classList.contains('current_note')){
          elements[i].classList.remove('current_note');
        }
        if(!elements[i].classList.contains('octave_cache')){
          elements[i].classList.add('octave_cache');
        }
    }
    //FIN NETTOYAGE


  }
  else{
    mode_accordeur.classList.remove('battements');
    mode_accordeur.classList.add('diapason');
    document.images["icone_mode"].src="img/accordeur/logo_diap.png";
    let next = document.getElementById('battement_procedure_next');
    next.style.display="none";
    let step = document.getElementById('battement_procedure_step');
    step.style.display="none";
    let previous = document.getElementById('battement_procedure_previous');
    previous.style.display="none";
    console.log("diapason");

    //NETTOYAGE de la div diapason
    let elements = document.getElementsByClassName('note_detection');
    for (let i = 0; i < elements.length; i++) {
        if(elements[i].classList.contains('current_note_battement')){
          elements[i].classList.remove('current_note_battement');
        }
        if(elements[i].classList.contains('current_note')){
          elements[i].classList.remove('current_note');
        }

    }

    elements = document.getElementsByClassName('note_detection_octave');
    for (let i = 0; i < elements.length; i++) {
        if(elements[i].classList.contains('current_note_battement')){
          elements[i].classList.remove('current_note_battement');
        }
        if(elements[i].classList.contains('current_note')){
          elements[i].classList.remove('current_note');
        }
        if(!elements[i].classList.contains('octave_cache')){
          elements[i].classList.add('octave_cache');
        }
    }
    //FIN NETTOYAGE

    selected_note_change('C');
  }
}

function display_infos(){
  var display_infos=document.getElementById("display_infos");
  if (display_infos.classList.contains('display')){
    display_infos.classList.remove('display');
    display_infos.classList.add('hide');
    display_infos.style.display="none";
  }
  else{
    display_infos.classList.remove('hide');
    display_infos.classList.add('display');
    display_infos.style.display="inline";
  }
}




UpdateHTML_apprendre();

jQuery(document).ready(function($) {
  $("a").click(function(event) {
    link=$(this).attr("href");
    $.ajax({
      url: link,
    })
    .done(function(html){
      console.log(link);
      $("#page").empty().append(html);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete ===============================================================");
    });
    return false;
  });
});



function UpdateHTML_apprendre(){
    console.log('----------------- APPRENDRE - AFFICHAGE DES CHAPITRES -------------------');
    const chapitre_boite = document.getElementById("conteneur_tous_chapitres");
    var message = "";
    let params = {};

    fetch('./src/bdd/liste_chapitres.php', {method: 'POST'})
    .then(response => response.json())
    .then( data => {
      var html = "";
      data.chapitres.forEach(function(chapitre){
                console.log(chapitre.nom);
                let function_string = "onmouseup="+"affichage_chapitre("+chapitre.id+",\'"+chapitre.nom+"\')";
                console.log(function_string);
                html += "\
                <div  class='conteneur_chapitre' id="+chapitre.id+" "+function_string+">\
                  <img class='logo_chapitre' src='"+chapitre.icone_chapitre_src+"' alt=''>\
                  <h3>"+chapitre.nom+"</h3>\
                </div>"
        })
        chapitre_boite.innerHTML = html;
      })
}

function affichage_chapitre(id, nom){
    console.log('coucou petite péruche des chapitres ' + id);
    var chapitre_id = document.getElementById('page');


    if(nom == "Définition"){
    console.log('coucou petite péruche "Definition" ' + id);
    chapitre_id.innerHTML = "\
            <div id='header_page'>\
              <h1>"+nom+"</h1>\
              <img class='logo_chapitre_entete' src='img/icon_apprendre_def.png' alt=''>\
            </div>\
            \
            <div id='contenu_chapitre'>\
            \
            <div class='sous_chapitre'>\
              <div class='sous_titre'>\
                <h4>QU'EST QU'UN TEMPÉRAMENT ?</h4>\
              </div>\
              <div id='texte-descriptif'>\
                    Le tempérament musical est l’ensemble des choix esthétiques faits au cours de l'accord d’un instrument de musique. La définition de Jean-Jacques Rousseau résume ce qu’on peut considérer comme un triple compromis que constitue un tempérament musical. Il s’agit de trouver une solution qui combine de la façon la plus satisfaisante, selon le contexte (le répertoire, les instruments concernés, etc.) : un paramètre physique et matériel (la “jouabilité” sur les instruments, sur un clavier, un manche avec des frettes, etc.), un paramètre musical (pouvoir moduler le plus librement et le plus loin possible), et un paramètre esthétique (conserver ou pas le plus possible d’intervalles purs “sans choquer l’oreille”).\
                    <br>\
                    <br>\
                    L’accord et le tempérament concernent les instruments à sons “fixes”, c’est-à-dire ceux dont les notes sont accordées avant que l’instrumentiste joue. Il s’agit entre autres des claviers tels que le clavecin, l’orgue ou le piano. Il ne faut pas confondre accord et justesse : on accorde un instrument avant d’en jouer, tandis que la justesse apparaît au moment du jeu. \
                    <br>\
                    <br>\
                    Avant le XIXe siècle, ces choix esthétiques correspondaient à des usages, des modes ou des habitudes en fonction des époques, des régions et d’autres facteurs influençant la musique elle-même. Il n’y avait donc pas de “choix” proprement dit mais des bonnes et des mauvaises manières d’accorder selon le lieu, le moment et les circonstances matérielles.\
                    <br>\
                    <br>\
                    A l’heure où les musiciens se penchent sur des répertoires anciens, il leur faut retrouver pour chacun de ces répertoires la manière d’accorder correspondant aux usages en vogue au moment de leur composition, condition nécessaire pour se rapprocher le plus possible de  leur réalité sonore.\
                    <br>\
                    <br>\
                    La notion de tempérament musical permet à un accordeur contemporain de rendre objectifs les critères qui faisait un bon accord pour un certain répertoire, et de donner un nom à cette manière d’accorder. Les tempéraments ont donc souvent des noms de compositeurs voire d’auteurs, des dates et des régions d’usages. Ces informations sont souvent moins absolues et véridiques que simplement utiles pour associer une technique d’accord à un répertoire.\
                    <br>\
                    <br>\
                    L’accordeur n’a jamais une liberté totale pour chaque note d’un instrument. Des contraintes réduisent les choix qu’il peut faire et permettent de résumer un tempérament en un petit nombre d’informations : un petit nombre de valeurs numériques qui décrivent certains intervalles.\
                    <br>\
                    <br>\
                    Ces valeurs représentent le “degré de fausseté” ou “tempérament” des intervalles. Un intervalle est dit “tempéré” s’il est accordé avec un certain degré de fausseté, s’il présente donc un écart par rapport à l’intervalle “pur” (cf. “les mathématiques”).\
                    <br>\
                    <br>\
                    Le degré de fausseté d’un intervalle peut être lui-même exprimé comme un intervalle : celui qui sépare l’une des notes de l’intervalle tempéré de sa valeur pure.\
                    <br>\
                    <br>\
                    Le clavier des instruments à sons fixes contient douze notes par octave. Toute l’information décrivant un tempérament sera donc contenue dans douze valeurs. \
                    On utilise aujourd’hui des valeurs numériques, avec lesquelles il est facile de faire des recherches ou de programmer. \
                    <br>\
                    <br>\
                    Pour manipuler ces valeurs, il est nécessaire de comprendre les mathématiques du tempérament musical.\
                    <br>\
                    <br>\
                    Les notes d'un clavier à 12 touches par octave comme le piano, le clavecin ou l'orgue sont reliées par un cycle de 12 quintes, et les tempéraments historiques ou plus récents pour clavier peuvent être définis comme une succession de 12 quintes de tailles différentes. Les principes sur lesquels ont été élaborés ces tempéraments ont évolué dans le temps ; un tempérament est non seulement une information précieuse sur l'évolution de la pensée théorique, mais surtout un témoignage sonore irréfutable et sans équivalent.\
              </div>\
           </div>\
           <div class='sous_chapitre'>\
              <div class='sous_titre'>\
                <h4>LES MATHÉMATIQUES</h4>\
              </div>\
              <div id='texte-descriptif'>\
              <div class='LaTeX'>\
              Depuis les auteurs grecs jusqu'au XIXe siècle, un intervalle est défini par un rapport de longueurs de cordes : par analogie, le rapport des sons est celui des longueurs des cordes qui les forment (ainsi le rapport mathématique est 1:2 pour l’octave, 2:3 pour la quinte, 3:4 pour la quarte, 4:5 pour la tierce majeure, 5:6 pour la tierce mineure, etc.). De nos jours, on emploie le rapport des fréquences des deux notes qui forment l'intervalle. On le nomme rapport acoustique, qui est l'inverse du rapport des longueurs. Les rapports acoustiques des intervalles simples à l'état pur sont ainsi les suivants : octave : \(2 \); quinte : \(3 \over 2\) ; quarte : \(4 \over 3\); tierce majeure : \\(5 \\over 4\\); tierce mineure \(6 \over 5\). Ces intervalles, auxquels on peut ajouter les intervalles complémentaires des tierces dans l'octave, à savoir la sixte mineure (\(8 \over 5\)) et la sixte majeure (\(5 \over 3\)) émettent un battement perceptible si on les écarte de leur valeur pure (correspondant à l’exactitude du rapport acoustique). Le rapport acoustique est indépendant de la fréquence de la note sur laquelle on construit l’intervalle. Pour obtenir la quinte d’une note quelconque, il faudra toujours multiplier la fréquence de cette note par \(3 \over 4\).\
              De la même manière qu’on ajoute les intervalles sur un clavier en élevant successivement une note d’un intervalle puis d’un autre, on peut calculer le rapport acoustique qui en résulte : le rapport acoustique de la somme de deux intervalles est le produit des rapports acoustiques des deux intervalles.\
              <br>\
              <br>\
              Ce principe s'illustre par la complémentarité des deux tierces dans la quinte :\
              $$\text{tierce majeure + tierce mineure} = {\text{quinte : } \frac{5}{4} \times \frac{6}{5} = \frac{16}{15}}$$\
              ou par la complémentarité de la quinte et de la quarte dans l'octave :\
              $$\text{quinte + quarte} = {\text{octave : } \frac{3}{2} \times \frac{4}{3} = 2}.$$\
              Soustraire un intervalle à un deuxième revient à diviser le rapport acoustique de ce dernier par celui du premier, soit par exemple :\
              $$\text{quarte - tierce majeure} = {\text{demi-ton : } \frac{4}{3} \div \frac{5}{4} = \frac{16}{15}}.$$\
      \
              Enfin le rapport acoustique correspondant à la division d’un intervalle en \(n\) intervalles égaux se calcule en prenant sa racine \(n-ième\) : C’est l’opération qui consiste à répartir le comma pythagoricien sur les douze quintes, qu’on abaisse toutes d’un petit intervalle dont la valeur est \(\sqrt[12]{ C_{p}}\). De même dans le tempérament mésotonique usuel, la tierce majeure est constituée de deux tons égaux dont le rapport acoustique est\(\sqrt{5 \over 4}\).\
              \
              La somme d'intervalles purs (qui, à l'audition, sont dénués de battement) entraîne la production de petits intervalles appelés commas qui affectent la pureté voire la jouabilité d'autres intervalles. Tempérer un intervalle, c'est l'éloigner de son état de pureté afin d'amoindrir voire annuler l'effet de ces commas.\
      Par extension, un tempérament est une façon d'organiser ces déviations de la pureté dans une échelle donnée.\
      \
      TemperApp utilise les commas syntonique et pythagoricien :\
              Le comma pythagoricien (Cp) est l’intervalle entre une note élevée de douze quintes pures et la même note élevée de 7 octaves. \
              $$C_{p} = { \frac{ (\frac{3}{2})^{12} }{2^{7}} } = \frac{3^{12}}{2^{19}}$$\
              Le comma syntonique (Cs) ou comma zarlinien est l’intervalle entre une note élevée de quatre quintes pures et la même note élevée d’une tierce majeure pure et d’une octave.\
              $$C_{s} = { \frac{ (\frac{3}{2})^{4} }{2 \frac{5}{4}} = \frac{81}{80} }$$\
      \
      Les deux nombres (fractions) représentant les intervalles Cs et Cp n’ont pas de relation arithmétique entre eux. Mais lorsqu’on les divise on remarque la quasi égalité suivante :\
      $$\\frac{C_{p}}{12} \\thickapprox \\frac{C_{s}}{11}$$\
                </div>\
              </div>\
           </div>\
      </div>"

      }
      if(nom == "Caractéristiques"){
        console.log('coucou petite péruche "Caractéristique" ' + id);
        chapitre_id.innerHTML = "\
                <script type='text/javascript' src='src/scripts/menu.js'></script>\
                <div id='header_page'>\
                  <h1 id='special_h1'>"+nom+"</h1>\
                  <img class='logo_chapitre_entete' id='special_logo' src='img/icon_apprendre_caracteristique.png' alt=''>\
                </div>\
                \
                <div id='contenu_chapitre'>\
                \
                <div class='sous_chapitre'>\
                  <div class='sous_titre'>\
                    <h4>LE TEMPÉRAMENT EGAL</h4>\
                  </div>\
                  <div id='texte-descriptif'>\
                  Le tempérament égal est une  norme européenne en vogue depuis le début du XIXe siècle. Il s’est imposé progressivement en dehors de la musique savante et de ses frontières d’origine. Aujourd’hui il est la norme dans la facture instrumentale et dans les musiques savantes autant que populaires. Toujours lié à cet héritage occidental récent, il coexiste avec d’autres normes dans des espaces culturels différents.\
                  <br>\
                  <br>\
                  Dans le tempérament égal, les douze quintes sont diminuées d’un douzième de comma pythagoricien. Les notes enharmoniques ont la même fréquence. L’intervalle séparant deux notes est constant.\
                  </div>\
               </div>\
               <div class='sous_chapitre'>\
                  <div class='sous_titre'>\
                    <h4>LES SYSTÈMES MÉSOTONIQUE</h4>\
                  </div>\
                  <div id='texte-descriptif'>\
                  Contrairement au tempérament de l’école pythagoricienne, qui se base sur la pureté des quintes, les systèmes mésotoniques se fondent sur une recherche de pureté des tierces.\
                  Dans les systèmes mésotoniques, chaque quinte est tempérée de la même  fraction de comma syntonique.\
                  <br>\
                  <br>\
                  La dernière ferme le cycle, elle est souvent démesurément agrandie et sa sonorité, de fait, très rude : pour cela elle est appelée “quinte du loup” . Dans le cycle des quintes, elle marque le passage des dièses aux bémols. Ainsi la quinte du loup est placée le plus couramment entre sol# et mib. Bien que cet intervalle soit une quinte pour l’accordeur, il est une sixte diminuée pour le compositeur ou l’interprète et sa fausseté est parfois employée comme effet musical.\
                  Les systèmes suivants emploient des fractions différentes de comma syntonique (ici n).\
                  </div>\
               </div>\
          </div>"
      }

      if(nom == "Physique"){
        console.log('coucou petite péruche ' + id);
        chapitre_id.innerHTML = "\
                <div id='header_page'>\
                <h1>"+nom+"</h1>\
                <img class='logo_chapitre_entete' src='img/icon_apprendre_physique.png' alt=''>\
                </div>\
                \
                <div id='contenu_chapitre'>\
                \
                <div class='sous_chapitre'>\
                  <div class='sous_titre'>\
                    <h4>QUELQUES NOTIONS DE PHYSIQUE</h4>\
                  </div>\
                  <div id='texte-descriptif'>\
                    Une corde pincée ou frappée vibre à certaines fréquences qui dépendent de sa tension, de sa masse par unité de longueur, de son diamètre et d’autres paramètres physiques. Les règles de calculs des vibrations s’appliquent de la même manière pour les tuyaux sonores.\
                    <br>\
                    <br>\
                    Lors de la vibration, plusieurs fréquences se superposent : une corde vibre sur toute sa longueur, mais chacune de ses subdivisions en 2, 3 et jusqu’à une infinité de parties vibrent aussi, à des fréquences plus élevées et à des amplitudes plus faibles. On nomme ces vibrations les 'partiels'.\
                    <br>\
                    <br>\
                    Lorsque deux cordes vibrent à la même fréquence, leurs vibrations se cumulent, il s’agit d’un unisson.\
                    <br>\
                    <br>\
                    Si l’on les désaccorde très légèrement, leurs vibrations se cumulent puis s’annulent alternativement. Cet unisson faux crée ce que l’on perçoit comme un battement. La fréquence de ce battement est la différence entre les deux fréquences de l’unisson faux.\
                    Cet unisson peut se produire entre les fréquences fondamentales de deux cordes, mais aussi entre ses fréquences partielles (ou harmoniques).\
                    Entre une corde vibrant à 200Hz et une corde vibrant une quinte plus haut à 300Hz il existe un unisson : le troisième partiel de la première corde vibre à 600Hz, à l’unisson du deuxième partiel de la deuxième corde.\
                    <br>\
                    <br>\
                    Si l’on désaccorde la seconde corde à 301Hz, on crée un unisson faux entre ces deux sons partiels : le premier vibre à 600Hz et le second à 602Hz. On percevra un battement à la fréquence de 2Hz (soit 2 battement par seconde).\
                    <br>\
                    <br>\
                    Les battements dépendent de la fréquence des notes sur laquelle on construit les intervalles.\
                    Si la tierce Do2-Mi2 bat à 2Hz et qu’on tempère la tierce Sol2-Si2 de la même quantité de comma, le battement de Sol-Si sera littéralement une quinte au dessus du battement de Do-Mi soit 3/2 x 2 = 3Hz. La quinte est ici l’intervalle qui sépare les deux intervalles tempérés.\
                    </div>\
                </div>\
              </div>"
      }
}

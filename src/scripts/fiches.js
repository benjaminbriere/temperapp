
UpdateHTML_fiches();

var current_temperament;

function recherche(query) {
  var recherche = query.toLowerCase();

  var new_temperaments = current_temperament.filter((temperaments) => {
      return (temperaments.name +' '+ temperaments.theoricien).toLowerCase().includes(recherche)
          || (temperaments.theoricien +' '+ temperaments.name).toLowerCase().includes(recherche)
  });

  UpdateHTML_fiches(new_temperaments);
}


function UpdateHTML_fiches(){
    console.log('----------------- FICHE - AFFICHAGE DES TEMPERAMENTS -------------------');
    const temp_boite = document.getElementById("conteneur_tous");
    var message = "";
    let params = {};

    fetch('./src/bdd/liste_temperaments.php', {method: 'POST'})
    .then(response => response.json())
    .then( data => {
      var html = "";
      data.temperaments.forEach(function(temperament){
                current_temperement = temperament; 
                console.log(temperament.nom);
                let function_string = "onmouseup="+"affichage_fiche_individuelle("+temperament.id+",\'"+temperament.nom+"\')";
                console.log(function_string);
                html += "\
                <div  class='marquepage1' id="+temperament.id+" "+function_string+">\
                     <img class='temperament_marque_page' src='img/marque-page_noir.png'  alt=''>\
                     <h6>"+temperament.nom+"</h6>\
                 </div>"
        })
        temp_boite.innerHTML = html;
      })
}

function UpdateTierce(nom){
    let params = {};
    console.log('--- UpdateTierce');
    params.nom = nom;
    fetch('./src/bdd/qualite_tierce.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then( data => {
      console.log('--- Lecture qualité tierce');
      var r = document.querySelector(':root');
      data.temperaments.forEach(function(temperament){
          r.style.setProperty('--tierce1', couleurTierce(temperament.C));
          r.style.setProperty('--tierce2', couleurTierce(temperament.G));
          r.style.setProperty('--tierce3', couleurTierce(temperament.D));
          r.style.setProperty('--tierce4', couleurTierce(temperament.A));
          r.style.setProperty('--tierce5', couleurTierce(temperament.E));
          r.style.setProperty('--tierce6', couleurTierce(temperament.B));
          r.style.setProperty('--tierce7', couleurTierce(temperament.Fd));
          r.style.setProperty('--tierce8', couleurTierce(temperament.Cd));
          r.style.setProperty('--tierce9', couleurTierce(temperament.Gd));
          r.style.setProperty('--tierce10', couleurTierce(temperament.Eb));
          r.style.setProperty('--tierce11', couleurTierce(temperament.Bb));
          r.style.setProperty('--tierce12', couleurTierce(temperament.F));
      })

    })
  }

  function couleurTierce(n){
    if(n< 0 || n>20){
      if(n< 0){
        return '#434343';
      }
      else{
        return '#8a0000';
      }
    }
    else{
      switch (n) {
        case '0':
          return '#45cbc7';
        break;
        case '1':
        return '#ffedd0';
        break;
        case '2':
          return '#ffe9c6';
        break;
        case '3':
        return '#ffe5bc';
        break;
        case '4':
          return '#ffe1b1';
        break;
        case '5':
        return '#ffdda7';
        break;
        case '6':
          return '#ffda9e';
        break;
        case '7':
          return '#ffd694';
        break;
        case '8':
        return '#ffd289';
        break;
        case '9':
          return '#ffc77d';
        break;
        case '10':
        return '#ffa669';
        break;
        case '11':
          return '#dd8654';
        break;
        case '12':
        return '#ff6741';
        break;
        case '13':
          return '#ff472d';
        break;
        case '14':
          return '#ff2416';
        break;
        case '15':
        return '#ff0604';
        break;
        case '16':
        return '#eb0000';
        break;
        case '17':
        return '#d40000';
        break;
        case '18':
        return '#bc0000';
        break;
        case '19':
        return '#a60000';
        break;
        case '20':
        return '#8a0000';
        break;
        default:
          return '#ffa669';
      }
    }
  }

  function UpdateQuinte(nom){
    let params = {};
    console.log('--- UpdateQuinte'+nom);
    params.nom = nom;
    fetch('./src/bdd/qualite_quinte.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then( data => {
      console.log('--- Lecture qualité Quinte');
      var r = document.querySelector(':root');
      data.temperaments.forEach(function(temperament){
          r.style.setProperty('--quinte1', couleurQuinte(temperament.C));
          r.style.setProperty('--quinte2', couleurQuinte(temperament.G));
          r.style.setProperty('--quinte3', couleurQuinte(temperament.D));
          r.style.setProperty('--quinte4', couleurQuinte(temperament.A));
          r.style.setProperty('--quinte5', couleurQuinte(temperament.E));
          r.style.setProperty('--quinte6', couleurQuinte(temperament.B));
          r.style.setProperty('--quinte7', couleurQuinte(temperament.Fd));
          r.style.setProperty('--quinte8', couleurQuinte(temperament.Cd));
          r.style.setProperty('--quinte9', couleurQuinte(temperament.Gd));
          r.style.setProperty('--quinte10', couleurQuinte(temperament.Eb));
          r.style.setProperty('--quinte11', couleurQuinte(temperament.Bb));
          r.style.setProperty('--quinte12', couleurQuinte(temperament.F));
      })
    })
  }

  function couleurQuinte(n){
    if(n< -12 || n>7){
      if(n< -12){
        return '#4f0000';
      }
      else{
        return '#161616';
      }
    }
    else{
      switch (n) {
        case '-12':
          return '#4f0000';
        break;
        case '-11':
          return '#7b0000';
        break;
        case '-10':
          return '#ae0000';
        break;
        case '-9':
          return '#e00000';
        break;
        case '-8':
          return '#ff0d08';
        break;
        case '-7':
          return '#ff321f';
        break;
        case '-6':
          return '#ff5435';
        break;
        case '-5':
          return '#ff764a';
        break;
        case '-4':
          return '#ff9960';
        break;
        case '-3':
          return '#ffbb75';
        break;
        case '-2':
          return '#ffd592';
        break;
        case '-1':
          return '#ffe5bb';
        break;
        case '0':
          return '#45cbc7';
        break;
        case '1':
          return '#b9b9b9';
        break;
        case '2':
          return '#9f9f9f';
        break;
        case '3':
          return '#7e7e7e';
        break;
        case '4':
          return '#666666';
        break;
        case '5':
          return '#4d4d4d';
        break;
        case '6':
          return '#333333';
        break;
        case '7':
          return '#161616';
        break;
        default:
          return '#E69138';
      }
    }
  }



function affichage_fiche_individuelle(id, nom){
    console.log('coucou petite péruche ' + id);
    var page_id = document.getElementById('page');

    UpdateTierce(nom);
    UpdateQuinte(nom);

    let params = {};
    console.log('--- UpdateCommas');
    params.nom = nom;
    fetch('./src/bdd/info_temperament.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then( data => {
      var html = "";
      data.temperaments.forEach(function(temperament){
                page_id.innerHTML = "\
    <link rel='stylesheet' href='./src/styles/style_roue_comma.css'>\
    <div id='header_page'>\
            <h1>"+nom+"</h1>\
        </div>\
        <link rel='stylesheet' href='./src/styles/style_roue_comma.css'>\
    <div id='cercle_complet'>\
        <div id='wheelTierce2'></div>\
        <div id='wheelQuinte2'></div>\
        <div id='wheelCache2'></div>\
        <div id='cercle_note_comma'>\
                <svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'\
                viewBox='0 0 250 250' style='enable-background:new 0 0 250 250;' xml:space='preserve'>\
                <style type='text/css'>\
                .st0{fill:#FFFFFF;}\
                .st1{font-family:'Karla-Bold';}\
                .st2{font-size:15.897px;}\
                .st3{enable-background:new    ;}\
                .st4{font-family:'MalgunGothicBold';}\
                .st5{font-size:13.8579px;}\
                .st6{font-family:'Karla-Medium';}\
                .st7{font-size:9.1279px;}\
                </style>\
                <g id='Notes_comma' xmlns:serif='http://www.serif.com/'>\
                <g id='fond'>\
                    <path id='fond_blanc' class='st0' d='M124.6,15C63,15,13.1,64.9,13.1,126.5C13.1,188.1,63,238,124.6,238\
                    c61.6,0,111.5-49.9,111.5-111.5C236.1,64.9,186.2,15,124.6,15z M124.6,212c-47.2,0-85.5-38.3-85.5-85.5\
                    c0-47.2,38.3-85.5,85.5-85.5c47.2,0,85.5,38.3,85.5,85.5C210.1,173.7,171.8,212,124.6,212z'/>\
                </g>\
                <g id='F_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1883.09,5110.55)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 8999.6455 3466.894)' class='st1 st2'>F</text>\
                </g>\
                <g id='Bb_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1746.76,5256.97)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 9196.4326 3154.7651)' class='st3'><tspan x='0' y='0' class='st1 st2'>B</tspan><tspan x='10.3' y='0' class='st4 st5'>♭</tspan></text>\
                </g>\
                <g id='Eb_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1550.37,5309.75)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 9198.9648 2784.7813)' class='st3'><tspan x='0' y='0' class='st1 st2'>E</tspan><tspan x='9.3' y='0' class='st4 st5'>♭</tspan></text>\
                </g>\
                <g id='G_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1366.97,5256.97)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 9020.0029 2474.8516)' class='st1 st2'>G#</text>\
                </g>\
                <g id='C_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1218.4,5112.72)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 8694.7227 2278.5322)' class='st1 st2'>C#</text>\
                </g>\
                <g id='F_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1166.53,4914.93)'>\
                    <text id='F_x23_' transform='matrix(0 1.7765 -1.7765 0 8319.25 2277.7197)' class='st1 st2'>F#</text>\
                </g>\
                <g id='B_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1218.4,4694.49)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 7951.73 2466.4077)' class='st1 st2'>B</text>\
                </g>\
                <g id='E_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1364.41,4553.08)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 7768.3408 2788.8096)' class='st1 st2'>E</text>\
                </g>\
                <g id='A_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1550.37,4503.37)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 7766.4165 3142.2185)' class='st1 st2'>A</text>\
                </g>\
                <g id='D_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1743.51,4553.08)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 7944.4497 3460.8752)' class='st1 st2'>D</text>\
                </g>\
                <g id='G_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1883.09,4694.49)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 8258.7314 3648.6082)' class='st1 st2'>G</text>\
                </g>\
                <g id='C_note_comma' transform='matrix(3.44676e-17,-0.562899,0.562899,3.44676e-17,-1934.96,4926.31)'>\
                    <text transform='matrix(0 1.7765 -1.7765 0 8696.4375 3651.2024)' class='st1 st2'>C</text>\
                </g>\
            </g>\
            <g id='comma'>\
            </g>\
            </svg>\
            </div>\
            </div>\
            <div class='table_commas' id='tableau_commas'>\
            <table>\
              <caption>COMMAS</caption>\
              <tbody>\
                <tr>\
                  <th scope='col'>F</th>\
                  <td id='comF'>"+temperament.F+"</td>\
                  <th scope='col'>B</th>\
                  <td id='comF'>"+temperament.B+"</td>\
                </tr>\
                <tr>\
                  <th scope='col'>C</th>\
                  <td id='comF'>"+temperament.C+"</td>\
                  <th scope='col'>F♯</th>\
                  <td id='comF'>"+temperament.Fd+"</td>\
                </tr>\
                <tr>\
                  <th scope='col'>G</th>\
                  <td id='comF'>"+temperament.G+"</td>\
                  <th scope='col'>C♯</th>\
                  <td id='comF'>"+temperament.Cd+"</td>\
                </tr>\
                <tr>\
                  <th scope='col'>D</th>\
                  <td id='comF'>"+temperament.D+"</td>\
                  <th scope='col'>G♯</th>\
                  <td id='comF'>"+temperament.Gd+"</td>\
                </tr>\
                <tr>\
                  <th scope='col'>A</th>\
                  <td id='comF'>"+temperament.A+"</td>\
                  <th scope='col'>B♭</th>\
                  <td id='comF'>"+temperament.Bb+"</td>\
                </tr>\
                <tr>\
                  <th scope='col'>E</th>\
                  <td id='comF'>"+temperament.E+"</td>\
                  <th scope='col'>E♭</th>\
                  <td id='comF'>"+temperament.Eb+"</td>\
              </tr>\
              </tbody>\
            </table>\
            </div>\
        <div id='partie-description'>\
          <div id='cartel'>\
              <div class='p2'>Théoricien : <span id='theoricien'>"+temperament.theoricien+"</span></div>\
              <div class='p2'>Epoque : <span id='theoricien'>"+temperament.periode+"</span></div>\
              <div class='p2'>Aire géographique : <span id='theoricien'>"+temperament.geographie+"</span></div>\
              <div class='p2'>Nature commatique : <span id='theoricien'> "+temperament.nature_commatique+"</span></div>\
              <div class='p2'>Particularité scructurelle : <span id='theoricien'>"+temperament.particularite+"</span></div>\
          </div>\
          <div id='texte-descriptif'>"+temperament.commentaire+"</div>\
        </div>"
      })
    })
  }

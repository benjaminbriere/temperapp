var procedure = "A4;E4-Gd4;D4-A4;B3-E4;A3-Cd4;Cd4:F4;F3-C4;C4-G3;G3-G4;G3-D4;A3:D4;C4-E4;E4:A3;E4-Gd4;Gd4-Gd3;Gd3:C3;G3-B3;B3:E4;B3-Eb4;Eb4:G4;D4-Fd4;Fd4-Fd3;Fd3-Bb3;Bb3:D4"
var res = procedure.split(";");
var num_procedure = 1;


var battement_type_procedure = document.getElementById('battement_type_procedure');

var current_note1 = current_note;
var current_note2 = current_note;

console.log("LONGUEUR DU TAB RES");
console.log(res.length);

var id_step = document.getElementById('battement_step');
id_step.innerHTML = num_procedure;

/*
res.forEach(function(i){
  console.log(i);
  if(i.split('-').length==2){
    console.log("ACCORDER");
  }
  else if(i.split(':').length==2){
    console.log("VERIFIER");
  }
  else{
    console.log("UNIQUE");
  }
});
*/


console.log(res);

function battement_procedure_next(){
  num_procedure++;
  id_step.innerHTML = num_procedure;
  console.log("Procédure n° "+(num_procedure)+" : "+res[num_procedure-1]);
  /*console.log(battement_note_conversion(res[num_procedure]));*/
  if(res[num_procedure-1].split('-').length==2){
    console.log("ACCORDER");
    battement_type_procedure.innerHTML='accorder';
  }
  else if(res[num_procedure-1].split(':').length==2){
    console.log("VERIFIER");
    battement_type_procedure.innerHTML='check';
  }
  else{
    console.log("UNIQUE");
    battement_type_procedure.innerHTML='accorder';
  }
}

function battement_procedure_previous(){
  num_procedure--;
  id_step.innerHTML = num_procedure;
  console.log("Procédure n° "+(num_procedure) +" : "+res[num_procedure-1]);
  if(res[num_procedure-1].split('-').length==2){
    console.log("ACCORDER");
    let note = 'note_'+res[num_procedure-1];
    console.log(note);
    battement_type_procedure.innerHTML='accorder';
  }
  else if(res[num_procedure-1].split(':').length==2){
    console.log("VERIFIER");
    battement_type_procedure.innerHTML='check';
  }
  else{
    console.log("UNIQUE");
    let note = 'note_'+res[num_procedure-1];
    console.log(note);
    battement_type_procedure.innerHTML='accorder';
  }
}

async function battement_procedure_step()
{
  console.log("Procédure n° "+(num_procedure)+" : "+res[num_procedure-1]);
  let temp_div = document.getElementById('battement_procedure_step');
  if(!temp_div.classList.contains('running')){
    temp_div.classList.add('running');
    battement_running = 1;
    await battement_procedure_step_execute();
    battement_running = 0;
    await sleep(500);
    temp_div.classList.remove('running');
  }
}

async function battement_procedure_step_execute(){
  if(res[num_procedure-1].split('-').length==2){
      let notes = res[num_procedure-1].split('-');
      console.log("ACCORDER");
      let n1 = notes[0];
      let n2 = notes[1];
      console.log("appel fonction 1");
      await battement_procedure_step_unique(n1,detect_octave(n1));
      await sleep(100);
      console.log("appel fonction 2");
      await battement_procedure_step_unique(n2,detect_octave(n2));
      await sleep(100);
      console.log("appel fonction 3");
      await battement_procedure_step_controler(n1,n2,detect_octave(n1),detect_octave(n2));
  }
  else if(res[num_procedure-1].split(':').length==2){
    let notes = res[num_procedure-1].split(':');
    console.log("VERIFIER ---S");
    let n1 = notes[0];
    let n2 = notes[1];
    battement_procedure_step_controler(n1,n2,detect_octave(n1),detect_octave(n2));
  }
  else{
    console.log("UNIQUE");
    battement_procedure_step_unique(res[num_procedure-1],detect_octave(res[num_procedure-1]));
  }
}

async function battement_procedure_step_unique(n,octave){

  let on_off = document.getElementById('on_off');
  if (on_off.classList.contains('off')){
    change_on_off();
  }

  console.log("--------------------------------- UNIQUE START");

  //o.stop();
  let battement_note_id = 'note_'+n;
  let frequency;
  //nettoyage des anciennes notes

  console.log("--------------------------------- UNIQUE NETTOYAGE");

  let elements = document.getElementsByClassName('current_note_battement');
  let size = elements.length
  for (let i = (size-1) ; i >= 0; i--) {
    elements[i].classList.remove('current_note_battement');
  }
  elements = document.getElementsByClassName('current_note');
  size = elements.length
  for (let i = (size-1) ; i >= 0; i--) {
    elements[i].classList.remove('current_note');
  }
  elements = document.getElementsByClassName('note_detection_octave');
  size = elements.length
  for (let i = (size-1) ; i >= 0; i--) {
      if(!elements[i].classList.contains('octave_cache')){
        elements[i].classList.add('octave_cache');
      }
  }
  //fin nettoyage anciennes notes



  if(octave == 4){
    console.log("--------------------------------- UNIQUE CAS OCTAVE 4");
    //console.log('octave 4');
    element = document.getElementById(battement_note_id);
    element.classList.add('current_note_battement');
    //affichage de l'octave
    let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
    battement_octave_id = battement_octave_id + '3';
    element=document.getElementById(battement_octave_id);
    element.classList.remove('octave_cache');
    frequency = Math.round(temperament_calcule[n]*100)/100+"Hz";
    await diapason_battement(Math.round(temperament_calcule[n]*100)/100);
  }
  else{
    console.log("--------------------------------- UNIQUE CAS OCTAVE 3");
    //affichage de l'octave
    let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
    let temp = n.substring(0, n.length - 1)+'4';
    frequency = Math.round(temperament_calcule[temp]*100)/2/100+"Hz";
    battement_octave_id = battement_octave_id + '3';
    element=document.getElementById(battement_octave_id);
    element.classList.remove('octave_cache');
    element.classList.add('current_note_battement');
    await diapason_battement(Math.round(temperament_calcule[temp]*100)/2/100);
  }
  let displayed_note= document.getElementById('selected_note_content');
  let displayed_note_frequency = document.getElementById('selected_note_frequency');
  displayed_note.innerHTML=n;
  displayed_note_frequency.innerHTML=frequency;
  console.log("FIN UNIQUE ==============================================================");
}

async function battement_procedure_step_accorder(n1,n2,octave1,octave2){

  let displayed_note= document.getElementById('selected_note_content');
  let displayed_note_frequency = document.getElementById('selected_note_frequency');

  let on_off = document.getElementById('on_off');
  if (on_off.classList.contains('off')){
    change_on_off();
  }
  //o.stop();
  //nettoyage
  let elements = document.getElementsByClassName('current_note_battement');
  let size = elements.length;
  for (let i = (size-1) ; i >= 0; i--) {
    elements[i].classList.remove('current_note_battement');
  }
  elements = document.getElementsByClassName('current_note');
  size = elements.length
  for (let i = (size-1) ; i >= 0; i--) {
    elements[i].classList.remove('current_note');
  }
  elements = document.getElementsByClassName('note_detection_octave');
  size = elements.length
  for (let i = (size-1) ; i >= 0; i--) {
      if(!elements[i].classList.contains('octave_cache')){
        elements[i].classList.add('octave_cache');
      }
  }

  if(octave1 == 4){
    let battement_note_id = 'note_'+n1;
    console.log('octave 4');
    element = document.getElementById(battement_note_id);
    element.classList.add('current_note');
    //affichage de l'octave
    let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
    battement_octave_id = battement_octave_id + '3';
    element=document.getElementById(battement_octave_id);
    if(element.classList.contains('octave_cache')){
      element.classList.remove('octave_cache');
    }
    diapason_battement(Math.round(temperament_calcule[n1]*100)/100);

    displayed_note.innerHTML=n1;
    displayed_note_frequency.innerHTML=Math.round(temperament_calcule[n1]*100)/100;
    await sleep(2000);
    //o.stop();
    if(octave2 == 4){
      battement_note_id = 'note_'+n2;
      console.log('octave 4');
      element = document.getElementById(battement_note_id);
      element.classList.add('current_note_battement');
      //affichage de l'octave
      battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
      battement_octave_id = battement_octave_id + '3';
      element=document.getElementById(battement_octave_id);
      if(element.classList.contains('octave_cache')){
        element.classList.remove('octave_cache');
      }
      diapason_battement2(Math.round(temperament_calcule[n2]*100)/100);

      displayed_note.innerHTML=n2;
      displayed_note_frequency.innerHTML=Math.round(temperament_calcule[n2]*100)/100;

      await sleep(2000);
      //o.stop();
      console.log('etape3');
      diapason_multiple(n1,n2,Math.round(temperament_calcule[n1]*100)/100,Math.round(temperament_calcule[n2]*100)/100);
    }
    else{
      console.log('octave 3');
      //affichage de l'octave
      let battement_note_id = 'note_'+n2;
      let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
      let temp = n2.substring(0, n2.length - 1)+'4';
      battement_octave_id = battement_octave_id + '3';
      element=document.getElementById(battement_octave_id);
      if(element.classList.contains('octave_cache')){
        element.classList.remove('octave_cache');
      }
      element.classList.add('current_note_battement');
      diapason_battement2(Math.round(temperament_calcule[temp]*100)/2/100);

      displayed_note.innerHTML=n2;
      displayed_note_frequency.innerHTML=Math.round(temperament_calcule[temp]*100)/2/100;

      await sleep(1000);
      //o.stop();
      if( !(n1.substring(0, n1.length - 1) == n2.substring(0, n2.length - 1) )){
        console.log('etape3');
        diapason_multiple(n1,n2,Math.round(temperament_calcule[n1]*100)/100,Math.round(temperament_calcule[temp]*100)/2/100);
      }
    }
  }
  else{
    //o.stop();
    console.log('octave 3');
    //affichage de l'octave
    let battement_note_id = 'note_'+n1;
    let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
    let temp = n1.substring(0, n1.length - 1)+'4';
    battement_octave_id = battement_octave_id + '3';
    element=document.getElementById(battement_octave_id);
    if(element.classList.contains('octave_cache')){
      element.classList.remove('octave_cache');
    }
    element.classList.add('current_note');
    diapason_battement(Math.round(temperament_calcule[temp]*100)/2/100);

    displayed_note.innerHTML=n1;
    displayed_note_frequency.innerHTML=Math.round(temperament_calcule[temp]*100)/2/100;

    await sleep(1000);
    if(octave2 == 4){
      //o.stop();
      battement_note_id = 'note_'+n2;
      console.log('octave 4');
      element = document.getElementById(battement_note_id);
      element.classList.add('current_note_battement');
      //affichage de l'octave
      battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
      battement_octave_id = battement_octave_id + '3';
      element=document.getElementById(battement_octave_id);
      if(element.classList.contains('octave_cache')){
        element.classList.remove('octave_cache');
      }
      diapason_battement2(Math.round(temperament_calcule[n2]*100)/100);

      displayed_note.innerHTML=n2;
      displayed_note_frequency.innerHTML=Math.round(temperament_calcule[n2]*100)/100;

      await sleep(1000);
      //o.stop();
      if( !(n1.substring(0, n1.length - 1) == n2.substring(0, n2.length - 1) )){
        console.log('etape3');
        diapason_multiple(n1,n2,Math.round(temperament_calcule[temp]*100)/2/100,Math.round(temperament_calcule[n2]*100)/100);
      }
    }
    else{
      //o.stop();
      console.log('octave 3');
      //affichage de l'octave
      let battement_note_id = 'note_'+n2;
      let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
      let temp = n2.substring(0, n2.length - 1)+'4';
      battement_octave_id = battement_octave_id + '3';
      element=document.getElementById(battement_octave_id);
      if(element.classList.contains('octave_cache')){
        element.classList.remove('octave_cache');
      }
      element.classList.add('current_note_battement');
      diapason_battement2(Math.round(temperament_calcule[temp]*100)/2/100);

      displayed_note.innerHTML=n2;
      displayed_note_frequency.innerHTML=Math.round(temperament_calcule[temp]*100)/2/100;

      await sleep(1000);
      //o.stop();
      console.log('etape3');
      let temp1 = n1.substring(0, n1.length - 1)+'4';
      diapason_multiple(n1,n2,Math.round(temperament_calcule[temp1]*100)/2/100,Math.round(temperament_calcule[temp]*100)/2/100);
    }
  }

  await sleep(3000);
  displayed_note.innerHTML = 'FIN FONCTION';
  console.log('-------------------------------------- FIN PROCEDURE ACCORDER');
}

async function battement_procedure_step_controler(n1,n2,octave1,octave2){
  let on_off = document.getElementById('on_off');
  if (on_off.classList.contains('off')){
    change_on_off();
  }
  //o.stop();
  //nettoyage des anciennes notes
  let elements = document.getElementsByClassName('current_note_battement');
  let size = elements.length;
  for (let i = (size-1) ; i >= 0; i--) {
    elements[i].classList.remove('current_note_battement');
  }
  elements = document.getElementsByClassName('current_note');
  size = elements.length;
  for (let i = (size-1) ; i >= 0; i--) {
    elements[i].classList.remove('current_note');
  }

  elements = document.getElementsByClassName('note_detection_octave');
  size = elements.length;
  for (let i = (size-1) ; i >= 0; i--) {
      if(!elements[i].classList.contains('octave_cache')){
        elements[i].classList.add('octave_cache');
      }
  }
  console.log(' n1 : '+n1+' n2 : '+n2);

  if(octave1 == 4){
    let battement_note_id = 'note_'+n1;
    console.log('octave 4');
    element = document.getElementById(battement_note_id);
    element.classList.add('current_note_battement');
    //affichage de l'octave
    let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
    battement_octave_id = battement_octave_id + '3';
    element=document.getElementById(battement_octave_id);
    element.classList.remove('octave_cache');
    if(octave2 == 4){
      battement_note_id = 'note_'+n2;
      console.log('octave 4');
      element = document.getElementById(battement_note_id);
      element.classList.add('current_note_battement');
      //affichage de l'octave
      battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
      battement_octave_id = battement_octave_id + '3';
      element=document.getElementById(battement_octave_id);
      if(element.classList.contains('octave_cache')){
        element.classList.remove('octave_cache');
      }
      console.log('etape3');
      await diapason_multiple(n1,n2,Math.round(temperament_calcule[n1]*100)/100,Math.round(temperament_calcule[n2]*100)/100);
    }
    else{
      console.log('octave 3');
      //affichage de l'octave
      let battement_note_id = 'note_'+n2;
      let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
      let temp = n2.substring(0, n2.length - 1)+'4';
      battement_octave_id = battement_octave_id + '3';
      element=document.getElementById(battement_octave_id);
      if(element.classList.contains('octave_cache')){
        element.classList.remove('octave_cache');
      }
      element.classList.add('current_note_battement');
      if( !(n1.substring(0, n1.length - 1) == n2.substring(0, n2.length - 1) )){
        console.log('etape3');
        let temp = n2.substring(0, n2.length - 1)+'4';
        await diapason_multiple(n1,n2,Math.round(temperament_calcule[n1]*100)/100,Math.round(temperament_calcule[temp]*100)/2/100);
      }
    }
  }
  else{
    console.log('octave 3');
    //affichage de l'octave
    let battement_note_id = 'note_'+n1;
    let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
    let temp = n1.substring(0, n1.length - 1)+'4';
    battement_octave_id = battement_octave_id + '3';
    element=document.getElementById(battement_octave_id);
    console.log(battement_octave_id);
    if(element.classList.contains('octave_cache')){
      element.classList.remove('octave_cache');
    }
    element.classList.add('current_note_battement');
    if(octave2 == 4){
      battement_note_id = 'note_'+n2;
      console.log('octave 4');
      element = document.getElementById(battement_note_id);
      element.classList.add('current_note_battement');
      //affichage de l'octave
      battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
      battement_octave_id = battement_octave_id + '3';
      element=document.getElementById(battement_octave_id);
      element.classList.remove('octave_cache');
      console.log('etape3');
      let temp = n1.substring(0, n1.length - 1)+'4';
      await diapason_multiple(n1,n2,Math.round(temperament_calcule[temp]*100)/2/100,Math.round(temperament_calcule[n2]*100)/100);
    }
    else{
      console.log('octave 3');
      //affichage de l'octave
      let battement_note_id = 'note_'+n2;
      let battement_octave_id = battement_note_id.substring(0, battement_note_id.length - 1);
      let temp = n2.substring(0, n2.length - 1)+'4';
      battement_octave_id = battement_octave_id + '3';
      element=document.getElementById(battement_octave_id);
      if(element.classList.contains('octave_cache')){
        element.classList.remove('octave_cache');
      }
      element.classList.add('current_note_battement');
      if( !(n1.substring(0, n1.length - 1) == n2.substring(0, n2.length - 1) )){
        console.log('etape3');
        let temp = n1.substring(0, n1.length - 1)+'4';
        let temp1 = n2.substring(0, n2.length - 1)+'4';
        await diapason_multiple(n1,n2,Math.round(temperament_calcule[temp]*100)/2/100,Math.round(temperament_calcule[temp1]*100)/2/100);
      }
    }
  }
  console.log('-------------------------------------- FIN PROCEDURE CHECK');
}



function detect_octave(n){
  if(n=='A4'||n=='B4'||n=='Bb4'||n=='C4'||n=='Cd4'||n=='D4'||n=='E4'||n=='Eb4'||n=='F4'||n=='Fd4'||n=='G4'||n=='Gd4'){
    return 4;
  }
  else{
    return 3;
  }
}

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

async function diapason_multiple(n1,n2,f1,f2){

  let pos1 = position_note(n1);
  let pos2 = position_note(n2);
  let freq1;
  let freq2;
  let vitesseBattement;

  if(pos2<pos1){
    if(pos1-pos2 == 4){
      freq1 = f2 * 5;
      freq2 = f1 * 4;
    }
    else if(pos1-pos2 == 5){
      freq1 = f2 * 4;
      freq2 = f1 * 3;
    }
    else{
      freq1 = f1 * 2;
      freq2 = f2 * 3;
    }
  }
  else{
    if(pos2-pos1 == 4){
      freq1 = f1 * 5;
      freq2 = f2 * 4;
    }
    else if(pos2-pos1 == 5){
      freq1 = f1 * 4;
      freq2 = f2 * 3;
    }
    else{
      freq1 = f2 * 2;
      freq2 = f1 * 3;
    }
  }
  vitesseBattement = freq2-freq1;
  let vitesseDisplay;
  if(vitesseBattement*60 >= 200){
    if(vitesseBattement*60 >= 600){
      vitesseDisplay = '//'
    }
    else{
      vitesseDisplay = 4 * Math.round(15 * vitesseBattement);
      vitesseDisplay = Math.round(vitesseDisplay/60*100)/100+"bps";
    }
  }
  else{
    vitesseDisplay = Math.round(60 * vitesseBattement);
    vitesseDisplay = Math.round(vitesseDisplay/60*100)/100+"bps";
  }

  console.log("FREQ1 : "+freq1+" FREQ2 : "+freq2+"  vitesse : "+vitesseBattement+"Hz  BPM : "+vitesseDisplay);

  let displayed_note= document.getElementById('selected_note_content');
  displayed_note.innerHTML= n1+'-'+n2;

  let displayed_note_frequency = document.getElementById('selected_note_frequency');
  displayed_note_frequency.innerHTML=vitesseDisplay;

  console.log('création de l audio context');
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  ctx = new AudioContext();
  o1 = ctx.createOscillator();
  o1.type = "sine";
  o1.frequency.value = freq1;
  o1.connect(ctx.destination);
  o2 = ctx.createOscillator();
  o2.type = "sine";
  o2.frequency.value = freq2;
  o2.connect(ctx.destination);
  o1.start(0);
  o2.start(0);
  //ctx.resume();
  await sleep(1000);
  o1.stop();
  o2.stop();
  ctx.close();

}

function position_note(n){
  switch (n) {
  case 'C3':
  return 0;
  break;
  case 'Cd3':
  return 1;
  break;
  case 'D3':
  return 2;
  break;
  case 'Eb3':
  return 3;
  break;
  case 'E3':
  return 4;
  break;
  case 'F3':
  return 5;
  break;
  case 'Fd3':
  return 6;
  break;
  case 'G3':
  return 7;
  break;
  case 'Gd3':
  return 8;
  break;
  case 'A3':
  return 9;
  break;
  case 'Bb3':
  return 10;
  break;
  case 'B3':
  return 11;
  break;

  case 'C4':
  return 12;
  break;
  case 'Cd4':
  return 13;
  break;
  case 'D4':
  return 14;
  break;
  case 'Eb4':
  return 15;
  break;
  case "E4":
  return 16;
  break;
  case 'F4':
  return 17;
  break;
  case 'Fd4':
  return 18;
  break;
  case 'G4':
  return 19;
  break;
  case 'Gd4':
  return 20;
  break;
  case 'A4':
  return 21;
  break;
  case 'Bb4':
  return 22;
  break;
  case 'B4':
  return 23;
  break;
  default:
  return 0;
  }
}

async function diapason_battement(freq){
  console.log(freq);
  console.log('création de l audio context pour battement');
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  ctx = new AudioContext();
  o = ctx.createOscillator();
  o.type = "sine";
  o.frequency.value = freq;
  g = ctx.createGain();
  g.gain.value = 1;
  o.start(0);
  o.connect(g);
  g.connect(ctx.destination);
  ctx.resume();
  await sleep(1000);
  o.stop();
  console.log('fin diapason battement 1');
  ctx.close();
}

function diapason_battement2(freq){
  console.log(freq);
  console.log('création de l audio context pour battement 2 ');
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  ctx2 = new AudioContext();
  o2 = ctx2.createOscillator();
  o2.type = "sine";
  o2.frequency.value = freq;
  g2 = ctx2.createGain();
  g2.gain.value = 1;
  o2.start(0);
  o2.connect(g2);
  g2.connect(ctx2.destination);
  ctx2.resume();
  o2.stop(1);
}

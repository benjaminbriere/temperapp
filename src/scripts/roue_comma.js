
  function drawComma() {

    console.log('------------------ROUE COMMA ---------------------------');
    var wheelDivComma = new wheelnav('wheelDivComma');

    wheelDivComma.wheelRadius = wheelDivComma.wheelRadius * 0.83;

    wheelDivComma.slicePathFunction = slicePath().DonutSlice;
    wheelDivComma.slicePathCustom = slicePath().DonutSliceCustomization();
    wheelDivComma.slicePathCustom.minRadiusPercent = 0.75;

    wheelDivComma.sliceSelectedPathFunction = slicePath().DonutSlice;
    wheelDivComma.sliceSelectedPathCustom = slicePath().DonutSliceCustomization();
    wheelDivComma.sliceSelectedPathCustom.minRadiusPercent = 0.75;

    wheelDivComma.animatetime = 0;
    wheelDivComma.animateeffect = 'linear';

    wheelDivComma.createWheel();
  }

  function drawNotes() {

    console.log('------------------ROUE Note (du comma) ---------------------------');
    var wheelDivNote = new wheelnav('wheelDivNote');

    wheelDivNote.wheelRadius = wheelDivNote.wheelRadius * 0.65;
    
    wheelDivNote.slicePathFunction = slicePath().DonutSlice;
    wheelDivNote.slicePathCustom = slicePath().DonutSliceCustomization();
    wheelDivNote.slicePathCustom.minRadiusPercent = 0.35;

    wheelDivNote.sliceSelectedPathFunction = slicePath().DonutSlice;
    wheelDivNote.sliceSelectedPathCustom = slicePath().DonutSliceCustomization();
    wheelDivNote.sliceSelectedPathCustom.minRadiusPercent = 0.35;

    wheelDivNote.animatetime = 0;
    wheelDivNote.animateeffect = 'linear';

    wheelDivNote.createWheel();
  }

  drawNotes();
  drawComma();




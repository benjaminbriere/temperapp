// LES VARIABLES GLOBALES //

var tab_temperament;
var temperament_name = document.getElementById('temperament1-select');
var temperament2_name = document.getElementById('temperament2-select');
var temperament_egal;
var temperament_calcule;
var ctx;
var te440 = '{ "A": 440, "B": 493.8833012561241 , "Bb": 466.1637615180899, "C": 261.62556530059874, "Cd": 277.1826309768722, "D": 293.66476791740763, "E": 329.62755691287, "Eb": 311.126983722081, "F": 349.22823143300394, "Fd": 369.99442271163446, "G": 391.9954359817493, "Gd": 415.3046975799451 } ';

async function run_comparaison(){
  console.log('---------------------- COMPARAISON ------------------------------');
  te440 = JSON.parse(te440);
  temperament_egal = te440;
  temperament_calcule = temperament_egal;
  await initHTML();
}

run_comparaison();

temperament_name.onchange = async function (){
  console.log("CHANGEMENT TEMPERAMENT");
  console.log(temperament_name.value);
  UpdateTierce(temperament_name.value);
  UpdateQuinte(temperament_name.value);
  UpdateCommas(temperament_name.value);
  temperament_calcule = await calculTemperament(temperament_egal, temperament_name.value);
  console.log(temperament_calcule);
}

temperament2_name.onchange = async function (){
    console.log("CHANGEMENT TEMPERAMENT");
    console.log(temperament2_name.value);
    UpdateTierce2(temperament2_name.value);
    UpdateQuinte1(temperament2_name.value);
    UpdateCommas1(temperament2_name.value);
    temperament_calcule = await calculTemperament(temperament_egal, temperament2_name.value);
    console.log(temperament_calcule);
  }


function UpdateTierce(nom){
  let params = {};
  console.log('--- UpdateTierce');
  params.nom = nom;
  fetch('./src/bdd/qualite_tierce.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(response => response.json())
  .then( data => {
    console.log('--- Lecture qualité tierce');
    var r = document.querySelector(':root');
    data.temperaments.forEach(function(temperament){
        r.style.setProperty('--tierce1', couleurTierce(temperament.C));
        r.style.setProperty('--tierce2', couleurTierce(temperament.G));
        r.style.setProperty('--tierce3', couleurTierce(temperament.D));
        r.style.setProperty('--tierce4', couleurTierce(temperament.A));
        r.style.setProperty('--tierce5', couleurTierce(temperament.E));
        r.style.setProperty('--tierce6', couleurTierce(temperament.B));
        r.style.setProperty('--tierce7', couleurTierce(temperament.Fd));
        r.style.setProperty('--tierce8', couleurTierce(temperament.Cd));
        r.style.setProperty('--tierce9', couleurTierce(temperament.Gd));
        r.style.setProperty('--tierce10', couleurTierce(temperament.Eb));
        r.style.setProperty('--tierce11', couleurTierce(temperament.Bb));
        r.style.setProperty('--tierce12', couleurTierce(temperament.F));
    })

  })
}

function UpdateTierce2(nom){
  let params = {};
  console.log('--- UpdateTierce');
  params.nom = nom;
  fetch('./src/bdd/qualite_tierce.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(response => response.json())
  .then( data => {
    console.log('--- Lecture qualité tierce');
    var r = document.querySelector(':root');
    data.temperaments.forEach(function(temperament){
        r.style.setProperty('--tierce_1', couleurTierce(temperament.C));
        r.style.setProperty('--tierce_2', couleurTierce(temperament.G));
        r.style.setProperty('--tierce_3', couleurTierce(temperament.D));
        r.style.setProperty('--tierce_4', couleurTierce(temperament.A));
        r.style.setProperty('--tierce_5', couleurTierce(temperament.E));
        r.style.setProperty('--tierce_6', couleurTierce(temperament.B));
        r.style.setProperty('--tierce_7', couleurTierce(temperament.Fd));
        r.style.setProperty('--tierce_8', couleurTierce(temperament.Cd));
        r.style.setProperty('--tierce_9', couleurTierce(temperament.Gd));
        r.style.setProperty('--tierce_10', couleurTierce(temperament.Eb));
        r.style.setProperty('--tierce_11', couleurTierce(temperament.Bb));
        r.style.setProperty('--tierce_12', couleurTierce(temperament.F));
    })

  })
}

function couleurTierce(n){
  if(n< 0 || n>20){
    if(n< 0){
      return '#434343';
    }
    else{
      return '#8a0000';
    }
  }
  else{
    switch (n) {
      case '0':
        return '#45cbc7';
      break;
      case '1':
      return '#ffedd0';
      break;
      case '2':
        return '#ffe9c6';
      break;
      case '3':
      return '#ffe5bc';
      break;
      case '4':
        return '#ffe1b1';
      break;
      case '5':
      return '#ffdda7';
      break;
      case '6':
        return '#ffda9e';
      break;
      case '7':
        return '#ffd694';
      break;
      case '8':
      return '#ffd289';
      break;
      case '9':
        return '#ffc77d';
      break;
      case '10':
      return '#ffa669';
      break;
      case '11':
        return '#dd8654';
      break;
      case '12':
      return '#ff6741';
      break;
      case '13':
        return '#ff472d';
      break;
      case '14':
        return '#ff2416';
      break;
      case '15':
      return '#ff0604';
      break;
      case '16':
      return '#eb0000';
      break;
      case '17':
      return '#d40000';
      break;
      case '18':
      return '#bc0000';
      break;
      case '19':
      return '#a60000';
      break;
      case '20':
      return '#8a0000';
      break;
      default:
        return '#ffa669';
    }
  }
}

function UpdateQuinte(nom){
  let params = {};
  console.log('--- UpdateQuinte');
  params.nom = nom;
  fetch('./src/bdd/qualite_quinte.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(response => response.json())
  .then( data => {
    console.log('--- Lecture qualité Quinte');
    var r = document.querySelector(':root');
    data.temperaments.forEach(function(temperament){
        r.style.setProperty('--quinte1', couleurQuinte(temperament.C));
        r.style.setProperty('--quinte2', couleurQuinte(temperament.G));
        r.style.setProperty('--quinte3', couleurQuinte(temperament.D));
        r.style.setProperty('--quinte4', couleurQuinte(temperament.A));
        r.style.setProperty('--quinte5', couleurQuinte(temperament.E));
        r.style.setProperty('--quinte6', couleurQuinte(temperament.B));
        r.style.setProperty('--quinte7', couleurQuinte(temperament.Fd));
        r.style.setProperty('--quinte8', couleurQuinte(temperament.Cd));
        r.style.setProperty('--quinte9', couleurQuinte(temperament.Gd));
        r.style.setProperty('--quinte10', couleurQuinte(temperament.Eb));
        r.style.setProperty('--quinte11', couleurQuinte(temperament.Bb));
        r.style.setProperty('--quinte12', couleurQuinte(temperament.F));
    })
  })
}

function UpdateQuinte1(nom){
  let params = {};
  console.log('--- UpdateQuinte');
  params.nom = nom;
  fetch('./src/bdd/qualite_quinte.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(response => response.json())
  .then( data => {
    console.log('--- Lecture qualité Quinte');
    var r = document.querySelector(':root');
    data.temperaments.forEach(function(temperament){
        r.style.setProperty('--quinte_1', couleurQuinte(temperament.C));
        r.style.setProperty('--quinte_2', couleurQuinte(temperament.G));
        r.style.setProperty('--quinte_3', couleurQuinte(temperament.D));
        r.style.setProperty('--quinte_4', couleurQuinte(temperament.A));
        r.style.setProperty('--quinte_5', couleurQuinte(temperament.E));
        r.style.setProperty('--quinte_6', couleurQuinte(temperament.B));
        r.style.setProperty('--quinte_7', couleurQuinte(temperament.Fd));
        r.style.setProperty('--quinte_8', couleurQuinte(temperament.Cd));
        r.style.setProperty('--quinte_9', couleurQuinte(temperament.Gd));
        r.style.setProperty('--quinte_10', couleurQuinte(temperament.Eb));
        r.style.setProperty('--quinte_11', couleurQuinte(temperament.Bb));
        r.style.setProperty('--quinte_12', couleurQuinte(temperament.F));
    })
  })
}

function couleurQuinte(n){
  console.log(n);
  if(n< -6 || n>5){
    if(n< -6){
      return '#85200C';
    }
    else{
      return '#434343';
    }
  }
  else{
    switch (n) {
      case '-6':
      case '-5':
        return '#A61C00';
      break;
      case '-4':
      case '-3':
        return '#CC0000';
      break;
      case '-2':
        return '#E69138';
      break;
      case '-1':
        return '#F6B26B';
      break;
      case '0':
        return '#CFE2F3';
      break;
      case '1':
      case '2':
        return '#999999';
      break;
      case '3':
      case '4':
        return '#666666';
      break;
      case '5':
        return '#434343';
      break;
      default:
        return '#E69138';
    }
  }
}

function UpdateCommas(nom) {
  var comA = document.getElementById('comA');
  var comBb = document.getElementById('comBb');
  var comB = document.getElementById('comB');
  var comC = document.getElementById('comC');
  var comCd = document.getElementById('comCd');
  var comD = document.getElementById('comD');
  var comEb = document.getElementById('comEb');
  var comE = document.getElementById('comE');
  var comF = document.getElementById('comF');
  var comFd = document.getElementById('comFd');
  var comG = document.getElementById('comG');
  var comGd = document.getElementById('comGd');
  var comma_temp1 = document.getElementById('comma_temp1');
  let params = {};
  console.log('--- UpdateCommas');
  params.nom = nom;
  fetch('./src/bdd/comma.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(response => response.json())
  .then( data => {
    comma_temp1.innerHTML = nom;
    data.temperaments.forEach(function(comma){
              console.log(comma);
              comA.innerHTML = comma.A;
              comBb.innerHTML = comma.Bb;
              comB.innerHTML = comma.B;
              comC.innerHTML = comma.C;
              comCd.innerHTML = comma.Cd;
              comD.innerHTML = comma.D;
              comEb.innerHTML = comma.Eb;
              comE.innerHTML = comma.E;
              comF.innerHTML = comma.F;
              comFd.innerHTML = comma.Fd;
              comG.innerHTML = comma.G;
              comGd.innerHTML = comma.Gd;
    });
  });
}

function UpdateCommas1(nom) {
  var com_A = document.getElementById('com_A');
  var com_Bb = document.getElementById('com_Bb');
  var com_B = document.getElementById('com_B');
  var com_C = document.getElementById('com_C');
  var com_Cd = document.getElementById('com_Cd');
  var com_D = document.getElementById('com_D');
  var com_Eb = document.getElementById('com_Eb');
  var com_E = document.getElementById('com_E');
  var com_F = document.getElementById('com_F');
  var com_Fd = document.getElementById('com_Fd');
  var com_G = document.getElementById('com_G');
  var com_Gd = document.getElementById('com_Gd');
  var comma_temp2 = document.getElementById('comma_temp2');
  let params = {};
  console.log('--- UpdateCommas');
  params.nom = nom;
  fetch('./src/bdd/comma.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(response => response.json())
  .then( data => {
    comma_temp2.innerHTML = nom;
    data.temperaments.forEach(function(comma){
              console.log(comma);
              com_A.innerHTML = comma.A;
              com_Bb.innerHTML = comma.Bb;
              com_B.innerHTML = comma.B;
              com_C.innerHTML = comma.C;
              com_Cd.innerHTML = comma.Cd;
              com_D.innerHTML = comma.D;
              com_Eb.innerHTML = comma.Eb;
              com_E.innerHTML = comma.E;
              com_F.innerHTML = comma.F;
              com_Fd.innerHTML = comma.Fd;
              com_G.innerHTML = comma.G;
              com_Gd.innerHTML = comma.Gd;
    });
  });
}

async function initHTML(){
  await UpdateHTML();
}

function loadJSON(url) {
    return new Promise(resolve => {
      var request = new XMLHttpRequest();
      request.open('GET', url);
      request.responseType = 'json';
      request.send();
      var result;
      var tab;
      request.onload = function() {
        result = request.response;
        tab = result;
        console.log(result);
        resolve(result);
      }
    });
 }

function UpdateHTML(){
   var message = "";
   let params = {};
   fetch('./src/bdd/liste_temperaments.php', {method: 'POST'})
   .then(response => response.json())
   .then( data => {

     data.temperaments.forEach(function(temperament){
         const info = "<option value ="+temperament.nom+">"+temperament.nom+"</option>";
         message = message + info;
     })
     temperament_name.innerHTML  = message;
     temperament2_name.innerHTML = message;
   })
   UpdateQuinte('Égal');
   UpdateQuinte1('Égal');
   UpdateTierce('Égal');
   UpdateTierce2('Égal');
 }

function affect_note(){
  for (var i = 0; i < 12; i++) {
    var name_id = 'wheelnav-wheelDiv-slice-'+i;
    var element = document.getElementById(name_id);

    switch (i) {

    case 0:
    element.classList.add('A');
    break;

    case 1:
    element.classList.add('E');
    break;

    case 2:
    element.classList.add('B');
    break;

    case 3:
    element.classList.add('F#');
    break;

    case 4:
    element.classList.add('C#');
    break;

    case 5:
    element.classList.add('G#');
    break;

    case 6:
    element.classList.add('E♭');
    break;

    case 7:
    element.classList.add('B♭');
    break;

    case 8:
    element.classList.add('F');
    break;

    case 9:
    element.classList.add('C');
    break;

    case 10:
    element.classList.add('G');
    break;

    case 11:
    element.classList.add('D');
    break;

    default:
      element.classList.add('A');
    }

    //console.log(name_id);
    //console.log(element);
  }
}

function display_infos(){
  var display_infos=document.getElementById("display_infos");
  if (display_infos.classList.contains('display')){
    display_infos.classList.remove('display');
    display_infos.classList.add('hide');
    display_infos.style.display="none";
  }
  else{
    display_infos.classList.remove('hide');
    display_infos.classList.add('display');
    display_infos.style.display="inline";
  }
}



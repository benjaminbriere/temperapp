async function calculTemperamentEgal(la){
  var params = {};
  params.la = la;
  try{
    let response;
    let res = await fetch('./src/bdd/calcul_temperament_egal.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then(data => {
      //console.log(data.egal);
      response = data.egal;
      //console.log(response);
      /*return 1;*/
    })
    return await response;
  }
  catch(error){
    console.log(error);
  }

}

async function calculTemperament(egal, nom){
  console.log("=== Calcul Temperament ===");
  var params = {};
  params.egal = egal;
  params.nom = nom;
  console.log(egal);
  console.log(nom);
  try{
    let response;
    let res = await fetch('./src/bdd/calcul_temperament.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then(data => {
      //console.log(data.egal);
      response = data.temperament;
      //console.log(response);
      /*return 1;*/
    })
    return await response;
  }
  catch(error){
    console.log(error);
  }
}

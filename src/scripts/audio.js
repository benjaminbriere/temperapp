window.addEventListener('load', function() {
    AudioContextCreator();

});


//
// window.onload = AudiContextCreator();


var ctx;
var o;
var g;
var temperament_name = document.getElementById('temperament-select');
var note;
var tab_temperament;
var multiple_note_counter = 0;
var gamme_mode_diapason;
var gamme_mode_battement;


/*
note_do.addEventListener('click',init);
*/


function UpdateHTML(){
  console.log(tab_temperament['members']);
  var message = "";
  for( const element of tab_temperament['members']){
    const info = "<option value ="+element['name']+">"+element['name']+"</option>";
    message = message + info;
  }
  console.log(message);
  temperament_name.innerHTML  = message;
}

async function AudioContextCreator(){
  console.log('création de l audio context');
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  ctx = new AudioContext();
  o = ctx.createOscillator();
  o.type = "sine";
  o.frequency.value = 440; //default
  o.start(0);
  g = ctx.createGain();
  g.gain.value = 0;
  o.connect(g);
  g.connect(ctx.destination);

  o1 = ctx.createOscillator();
  o1.type = "sine";
  o1.frequency.value = 0; //default
  o1.start(0);
  g1 = ctx.createGain();
  g1.gain.value = 0;
  o1.connect(g1);
  g1.connect(ctx.destination);

  tab_temperament = await loadJSON('temperapp/src/temperaments.json');
  console.log("audio script");
  console.log(tab_temperament['members']);
  UpdateHTML();
}

function note_detection(note){
  gamme_mode_diapason = document.getElementById('gamme_mode_diapason');
  gamme_mode_battement = document.getElementById('gamme_mode_battement');

  note_id = document.getElementById(note);
  console.log('temperament : '+temperament_name.value);
  console.log('note choisie '+note);

  if(gamme_mode_diapason.classList.contains('mode_selected')){

    ctx.resume();
    if(note_id.classList.contains('play')){
      console.log('retrait de la classe play');
      note_id.classList.remove('play');
      g.gain.value=0;
    }
    else{
      remove_play(note);
      note_id.classList.add('play');
      console.log('ajout de la classe play : ');
      note_freqs_value(note,0);
      g.gain.value=1;
    }
  }
  else{
    ctx.resume();
    if(multiple_note_counter == 0){
      note_id.classList.add('first_note_selected');
      multiple_note_counter++;

      g.gain.value=0;
      g1.gain.value=0;

    }
    else{
      if(multiple_note_counter == 1){
        if(note_id.classList.contains('first_note_selected')){
          note_id.classList.remove('first_note_selected');
          multiple_note_counter--;

          g.gain.value=0;
          g1.gain.value=0;

        }
        else{
          note_id.classList.add('second_note_selected');
          multiple_note_counter++;
          //CREATION DU Battement

          var x = document.getElementsByClassName('first_note_selected');

          note_freqs_value(x[0].id, 0);

          console.log("DEUXIEME NOTE : " + note_id.id);

          note_freqs_value(note_id.id, 1);

          g.gain.value=0.7;
          g1.gain.value=0.7;

          //
        }
      }
      else{
        if(note_id.classList.contains('first_note_selected')){
          note_id.classList.remove('first_note_selected');
          multiple_note_counter--;
          var x = document.getElementsByClassName('second_note_selected');
          x[0].classList.add('first_note_selected');
          x[0].classList.remove('second_note_selected');

          g.gain.value=0;
          g1.gain.value=0;

        }
        else{
          if(note_id.classList.contains('second_note_selected')){
            note_id.classList.remove('second_note_selected');
            multiple_note_counter--;

            g.gain.value=0;
            g1.gain.value=0;

          }
          else{
            var x = document.getElementsByClassName('first_note_selected');
            x[0].classList.remove('first_note_selected');
            var y = document.getElementsByClassName('second_note_selected');
            y[0].classList.add('first_note_selected');
            y[0].classList.remove('second_note_selected');
            note_id.classList.add('second_note_selected');

            //CREATION DU Battement

            ctx.resume();

            note_freqs_value(y[0].id, 0);
            note_freqs_value(note_id.id, 1);

            g.gain.value=0.7;
            g1.gain.value=0.7;

            //

          }
        }
      }
    }
  }

}

function change_gamme_mode(mode){
  mode_id = document.getElementById(mode);
  if(!(mode_id.classList.contains('mode_selected'))){
    var x = document.getElementsByClassName('mode_selected');
    for (var i = 0; i < x.length; i++) {
      if(x[i].classList.contains('mode_selected')){
        x[i].classList.remove('mode_selected');
      }
    }
    mode_id.classList.add('mode_selected');
  }
}

function multiple_note_detection(note){
  if(multiple_note_counter == 0){
    multiple_note_counter++;
    note_id = document.getElementById(note);
  }
}

function remove_play(note){
  var x = document.getElementsByClassName('note');
  for (var i = 0; i < x.length; i++) {
    if(x[i].classList.contains('play')){
      x[i].classList.remove('play');
    }
  }
}

function loadJSON(url) {
    return new Promise(resolve => {
      var request = new XMLHttpRequest();
      request.open('GET', url);
      request.responseType = 'json';
      request.send();
      var result;
      var tab;
      request.onload = function() {
        result = request.response;
        tab = result;
        console.log(result);
        resolve(result);
      }
    });
 }

function note_freqs_value(note,stream){

  for( const element of tab_temperament['members']){
    if(element['name']==temperament_name.value){
      console.log('la note est : '+note);
      switch (note) {

        case 'do':
          console.log('DO !');
          if(stream == 0){
            o.frequency.value = element['do'];
          }
          else if(stream == 1){
            o1.frequency.value = element['do'];
          }
          break;

        case 'do_#':
          console.log('DO_# !');
          if(stream == 0){
            o.frequency.value = element['do_#'];
          }
          else if(stream == 1){
            o1.frequency.value = element['do_#'];
          }
          break;

        case 're':
          console.log('RE !');
          if(stream == 0){
            o.frequency.value = element['re'];
          }
          else if(stream == 1){
            o1.frequency.value = element['re'];
          }
          break;

        case 'mi_b':
          console.log('Mi_b !');
          if(stream == 0){
            o.frequency.value = element['mi_b'];
          }
          else if(stream == 1){
            o1.frequency.value = element['mi_b'];
          }
          break;

        case 'mi':
          console.log('MI !');
          if(stream == 0){
            o.frequency.value = element['mi'];
          }
          else if(stream == 1){
            o1.frequency.value = element['mi'];
          }
          break;

        case 'fa':
          console.log('FA !');
          if(stream == 0){
            o.frequency.value = element['fa'];
          }
          else if(stream == 1){
            o1.frequency.value = element['fa'];
          }
          break;

        case 'fa_#':
          console.log('FA_# !');
          if(stream == 0){
            o.frequency.value = element['fa_#'];
          }
          else if(stream == 1){
            o1.frequency.value = element['fa_#'];
          }
          break;

        case 'sol':
          console.log('SOL !');
          if(stream == 0){
            o.frequency.value = element['sol'];
          }
          else if(stream == 1){
            o1.frequency.value = element['sol'];
          }
          break;

        case 'sol_#':
          console.log('SOL_# !');
          if(stream == 0){
            o.frequency.value = element['sol_#'];
          }
          else if(stream == 1){
            o1.frequency.value = element['sol_#'];
          }
          break;

        case 'la':
          console.log('LA !');
          if(stream == 0){
            o.frequency.value = element['la'];
          }
          else if(stream == 1){
            o1.frequency.value = element['la'];
          }
          break;

        case 'si_b':
          console.log('SI_b !');
          if(stream == 0){
            o.frequency.value = element['si_b'];
          }
          else if(stream == 1){
            o1.frequency.value = element['si_b'];
          }
          break;

        case 'si':
          console.log('SI !');
          if(stream == 0){
            o.frequency.value = element['si'];
          }
          else if(stream == 1){
            o1.frequency.value = element['si'];
          }
          break;

        case 'do2':
          console.log('DO2 !');
          if(stream == 0){
            o.frequency.value = element['do2'];
          }
          else if(stream == 1){
            o1.frequency.value = element['do2'];
          }
          break;

        default:
          console.log('default !');
          o.frequency.value = 440.00;
          break;
      }
    }
  }


/*
  console.log('la note est : '+note);
  switch (note) {
    case 'do':
      console.log('DO !');
      o.frequency.value = 261.63;
      break;
    case 're':
      console.log('RE !');
      o.frequency.value = 293.66;
      break;
    case 'mi':
      console.log('MI !');
      o.frequency.value = 329.63;
      break;
    case 'fa':
      console.log('FA !');
      o.frequency.value = 349.23;
      break;
    case 'sol':
      console.log('SOL !');
      o.frequency.value = 392.00;
      break;
    case 'la':
      console.log('LA !');
      o.frequency.value = 440.00;
      break;
    case 'si':
      console.log('SI !');
      o.frequency.value = 493.88;
      break;
    default:
      console.log('default !');
      o.frequency.value = 440.00;
      break;
  }
*/
}

<?php
include('./connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
  $json = file_get_contents('php://input');
  $data = json_decode($json, TRUE);
  $la = $data['la'];

  (float)$A = $la;
  $Bb = $A*(pow(2,+1/12));
  $B = $Bb*(pow(2,1/12));
  $Gd = $A*(pow(2,-1/12));
  $G = $Gd*(pow(2,-1/12));
  $Fd = $G*(pow(2,-1/12));
  $F = $Fd*(pow(2,-1/12));
  $E = $F*(pow(2,-1/12));
  $Eb = $E*(pow(2,-1/12));
  $D = $Eb*(pow(2,-1/12));
  $Cd = $D*(pow(2,-1/12));
  $C = $Cd*(pow(2,-1/12));

  $temperament = (array(
      'A4' => $A,
      'Bb4' => $Bb,
      'B4' => $B,
      'C4' => $C,
      'Cd4' => $Cd,
      'D4' => $D,
      'Eb4' => $Eb,
      'E4' => $E,
      'F4' => $F,
      'Fd4' => $Fd,
      'G4' => $G,
      'Gd4' => $Gd,
      ));

  $reponse = (array(
    'egal' => $temperament,
  ));

  echo json_encode($reponse);

  header('Content-Type: application/json; charset=UTF-8');
  header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}

?>

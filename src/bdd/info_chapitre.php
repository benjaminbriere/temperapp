<?php
include('./connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
  $json = file_get_contents('php://input');
  $data = json_decode($json, TRUE);
  $nom = $data['nom'];

  $contenu_chapitre = $con->prepare("SELECT introduction, partie1_titre, partie2_titre, partie1_paragraphe, partie2_paragraphe FROM temperapp.contenu_chapitres
    JOIN liste_chapitres ON contenu_chapitres.id_chapitres = liste_chapitres.id
    WHERE liste_chapitres.nom_chapitre = '$nom'");
  $contenu_chapitre->execute();

  $contenu_chapitre = $contenu_chapitre->fetchAll();


  $i = 0;
  foreach ($contenu_chapitre as $res) {
    $chapitres[$i] = (array(
        'introduction' => $res['introduction'],
        'partie1_titre' => $res['partie1_titre'],
        'partie2_titre' => $res['partie2_titre'],
        'partie1_paragraphe' => $res['partie1_paragraphe'],
        'Partie2_paragraphe' => $res['partie2_paragraphe'],
        ));
        $i++;
      }
  $reponse2 = (array(
    'chapitres' => $chapitres,
  ));

  echo json_encode($reponse2);

  header('Content-Type: application/json; charset=UTF-8');
  header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}

?>

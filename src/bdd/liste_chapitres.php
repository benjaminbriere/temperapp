<?php
include('./connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){

  $request = $con->prepare("SELECT * FROM temperapp.liste_chapitres");
  $request->execute();

  $request = $request->fetchAll();

  $i = 0;
  foreach ($request as $temp){
    $chapitres[$i] = (array(
      'id' => $temp['id'],
      'nom' => $temp['nom_chapitre'],
      'id_chapitres' => $temp['id_chapitres'],
      'icone_chapitre_src' => $temp['icone_chapitre_src'],
      ));
      $i++;
    }
  $reponse = (array(
    'chapitres' => $chapitres,
  ));

  echo json_encode($reponse);

  header('Content-Type: application/json; charset=UTF-8');
  header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}

?>

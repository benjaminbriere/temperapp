<?php
include('./connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
  $json = file_get_contents('php://input');
  $data = json_decode($json, TRUE);
  $egal = $data['egal'];
  $nom = $data['nom'];

  $request = $con->prepare("SELECT A,Bb,B,C,Cd,D,Eb,E,F,Fd,G,Gd FROM temperapp.deviation
    JOIN temperament ON deviation.id = temperament.id
    JOIN liste_temperaments ON liste_temperaments.id_temperament = temperament.id
    WHERE liste_temperaments.nom_temperament = '$nom'");
  $request->execute();

  $request = $request->fetchAll();
  $i = 0;
  foreach ($request as $temp){
    $temperament= (array(
      'A4' => $egal['A4'] * pow(2,($temp['A']/1200)),
      'Bb4' => $egal['Bb4'] * pow(2,($temp['Bb']/1200)),
      'B4' => $egal['B4'] * pow(2,($temp['B']/1200)),
      'C4' => $egal['C4'] * pow(2,($temp['C']/1200)),
      'Cd4' => $egal['Cd4'] * pow(2,($temp['Cd']/1200)),
      'D4' => $egal['D4'] * pow(2,($temp['D']/1200)),
      'Eb4' => $egal['Eb4'] * pow(2,($temp['Eb']/1200)),
      'E4' => $egal['E4'] * pow(2,($temp['E']/1200)),
      'F4' => $egal['F4'] * pow(2,($temp['F']/1200)),
      'Fd4' => $egal['Fd4'] * pow(2,($temp['Fd']/1200)),
      'G4' => $egal['G4'] * pow(2,($temp['G']/1200)),
      'Gd4' => $egal['Gd4'] * pow(2,($temp['Gd']/1200)),
      ));
      $i++;
    }

  $reponse = (array(
    'temperament' => $temperament,
  ));

  echo json_encode($reponse);

  header('Content-Type: application/json; charset=UTF-8');
  header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}

?>

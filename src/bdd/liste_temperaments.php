<?php
include('./connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){

  $request = $con->prepare("SELECT * FROM temperapp.liste_temperaments");
  $request->execute();

  $request = $request->fetchAll();

  $i = 0;
  foreach ($request as $temp){
    $temperaments[$i] = (array(
      'id' => $temp['id'],
      'nom' => $temp['nom_temperament'],
      'id_temperament' => $temp['id_temperament']
      ));
      $i++;
    }
  $reponse = (array(
    'temperaments' => $temperaments,
  ));

  echo json_encode($reponse);

  header('Content-Type: application/json; charset=UTF-8');
  header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}

?>

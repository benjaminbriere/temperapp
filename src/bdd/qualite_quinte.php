<?php
include('./connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
  $json = file_get_contents('php://input');
  $data = json_decode($json, TRUE);
  $nom = $data['nom'];

  $request = $con->prepare("SELECT A,Bb,B,C,Cd,D,Eb,E,F,Fd,G,Gd FROM temperapp.quinte
    JOIN temperament ON quinte.id = temperament.id
    JOIN liste_temperaments ON liste_temperaments.id_temperament = temperament.id
    WHERE liste_temperaments.nom_temperament = '$nom'");
  $request->execute();

  $request = $request->fetchAll();

  $i = 0;
  foreach ($request as $temp){
    $temperaments[$i] = (array(
      'A' => $temp['A'],
      'Bb' => $temp['Bb'],
      'B' => $temp['B'],
      'C' => $temp['C'],
      'Cd' => $temp['Cd'],
      'D' => $temp['D'],
      'Eb' => $temp['Eb'],
      'E' => $temp['E'],
      'F' => $temp['F'],
      'Fd' => $temp['Fd'],
      'G' => $temp['G'],
      'Gd' => $temp['Gd'],
      ));
      $i++;
    }
  $reponse = (array(
    'temperaments' => $temperaments,
  ));

  echo json_encode($reponse);

  header('Content-Type: application/json; charset=UTF-8');
  header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}

?>

self.addEventListener("install", e =>  {
    e.waitUntil(
        caches.open("static").then(cache => {
            return cache.addAll([
                './',
                './src/pages',
                './img',
                './src/styles/master.css',
                './src/styles/style_roue.css',
                './src/scripts/index.js',
            ]);
        })
    );
});

self.addEventListener("fetch", e => {
    e.respondWith(
        caches.match(e.request).then(response => {
            return response || fetch(e.request);
        })
    );
});

# TEMPERapp 

## Présentation

TEMPERapp est une application à destination des musicien·ne·s, qui a pour vocation la réactualisation 
de la pratique de l’accord et des tempéraments anciens. Elle constitue un outil scientifique et 
pédagogique favorisant l’apprentissage de l’accord à l’oreille, dont le principe se base sur la 
notion de rapport intervallaire. 

## Installation

TEMPERapp est une Progressive Web App et est donc assessible depuis le WEB ou en l'installant sur son PC ou son smartphone. Elle néccessite un navigateur à jour.

## Crédit et sources

TEMPERapp est issue du travail de recherche réalisé par :
- **Elisa Barbessi**, professeur de clavecin et histoire de la musique au CRR du Grand-Avignon, à l’initiative du projet. Doctorante à l’université Sorbonne, membre d’IReMus, Elisa est directrice artistique d’ARTEMIDA,
- **Jérôme Bertier**, pianiste, claveciniste et organiste, professeur au conservatoire d’Auxerre,
- **Pierre Cazes**, claveciniste. Il enseigne au CNSMDP l'histoire, la théorie et la pratique des tempéraments, et la basse continue. Il est professeur de clavecin au CRR93 (Aubervilliers/La Courneuve),
- **Franck Jedrzejewski**, chercheur au CEA, docteur habilité en musicologie et philosophie. Ancien Vice-président du Collège International de Philosophie, a publié une vingtaine d'ouvrages.
- **Théodora Psychoyou**, IReMus - Sorbonne Université.
 L’application est développée par des étudiants de l’école d’ingénieur IMAC 
Fabian Adam, Benjamin Briere, Daphné Chamot-Rooke, Sterenn Fonseca
encadrés par Jérôme Belloin, informaticien.


Le projet bénéficie de nombreux soutiens sous la forme de ressources humaines et techniques : 

- Le Conservatoire National Supérieur de Musique et de Danse de Paris ,
- le laboratoire "Lutheries - Acoustique - Musique" (LAM),
- l'Institut Jean le Rond d’Alembert,
- IReMus,
- Revue Musicorum,
- l’Association ARTEMIDA,
- le Conservatoire du Grand-Avignon.
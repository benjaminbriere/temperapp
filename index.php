<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="img/logapp.png">
    <meta name="theme-color" content="#e91e44">
    <title>Temperapp</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="./src/styles/normalize.css"></link>
    <link rel="stylesheet" href="./src/styles/master.css"></link>
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="./src/styles/style_roue.css">
    <link rel="icon" href="img/logapp.png">
    <script type="text/javascript" src="src/scripts/index.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="src/scripts/menu.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script type="text/javascript" id="MathJax-script" async
    src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js">
    </script>
</head>

    <body>

        <div id="page">

        </div>

    </body>

    <div id="link">
        <?php include 'src/pages/menu.php'?>
    </div>
</html>
